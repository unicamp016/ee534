#include <Arduino.h>

#include "common.h"
#include "serial.h"
#include "timerOne.h"

#define Versao "LabUno4"

uint32_t f8 = 100;
uint32_t t8 = 0;

void setup() {
  tm1_setup();
  Serial.begin(500000);
}

void loop() {
  serial_loop();
}

// 500.000 / 800 * 10 = 62.5 
