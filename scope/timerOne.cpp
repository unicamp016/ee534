/* This timer setup is ONLY for controlling the digital output of arduino's
 * 8th pin. The logic for the A/D converter measurements is at serial.cpp.
 */
#include <Arduino.h>
#include "TimerOne.h"

#include "common.h"

uint32_t f8_cnt=0;

/*
 * The callback is called every 500us (i.e. 0.5ms). In practice, t8 is the
 * counter for the number of interrupts that we should wait for before
 * executing the callback function. In theory, t8 should be the period between
 * successive callback calls. Since the minimum value for t8 is 1, this means
 * that in theory, the scope supports at most 1000Hz sampling rate, which means
 * that we can inspect at most a sine wave of 100Hz. In practice, the scope
 * is sampling at 2000Hz. This i
 */
void callback(){
  if(f8_cnt>=t8){
    digitalWrite(8, digitalRead(8) ^ 1);
    f8_cnt=1;
  }else{
    f8_cnt++;
  }
}

void tm1_setup(){
  pinMode(8,OUTPUT);
  Timer1.initialize(500); // Inicializa o Timer1 e configura para um período de 0,5 segundos
  Timer1.attachInterrupt(callback); // Configura a função callback() como a função para ser chamada a cada interrupção do Timer1
  t8=1000/f8;
}
