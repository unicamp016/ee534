\section{Comparador}\label{sec:comparator}
Este experimento envolve o uso de um comparador para gerar um sinal de PWM.
Vamos usar um amplificador do CI LM324~\cite{amp-datasheet} em malha aberta e
regular o DC (\emph{duty cycle}) do sinal PWM através da tensão na saída não
inversora do amplificador.

\subsection{Montagem}
A onda triangular de \SI{100}{\hertz} foi gerada a partir do celular usando o
aplicativo ``Frequency Generator''~\cite{}. Usando um
cabo de aúdio P2, foi possível medir o sinal $v_i$ com o oscilloscópio DIY\@, de
onde obteve-se a Figura~\ref{fig:vi}. Note que não é possível medir a parte
negativa do sinal, visto que o conversor AD do arduino tem range de entrada de 0
a \SI{5}{\volt}.

\begin{figure}
    \centering
    \includegraphics[width=0.6\textwidth]{./images/comparator/audio_triangle_wave_100hz.png}
    \caption{Onda triangular de \SI{100}{\hertz} capturada pelo conversor $A_1$
    do arduino. Temos que a tensão de pico da onda triangular é cerca de $V_p =
    \SI{500}{\milli\volt}$.}\label{fig:vi}
\end{figure}

Para que possamos medir o sinal completo, devemos aplicar um offset DC no sinal
de áudio de forma que possamos medir toda sua excursão no com o nosso
oscilloscópio. Usando o circuito da Figura~\ref{fig:bias}, foi possível obter um
sinal deslocado em \SI{2,5}{\volt}, com $V_{pp} = \SI{1}{\volt}$. O circuito usa
capacitores de acoplamento para eliminar o ruído da fonte $V_{CC} = \SI{5}{\volt}$ e eliminar
qualquer componente DC do sinal de áudio $v_i$. A onda resultante $v_I$ é dada
pela Figura~\ref{fig:vI}

\begin{figure}
    \centering
    \includegraphics[width=0.6\textwidth]{./images/comparator/bias-offset.pdf}
    \caption{Circuito usado para aplicar um offset DC de \SI{2,5}{\volt} no
    sinal de áudio $v_i$, obtendo-se o sinal de saída $v_I$ com tensão $V_{pp} =
    \SI{1}{\volt}$ e componente DC de \SI{2,5}{\volt}, variando entre
    \SI{2,0}{\volt} e \SI{3,0}{\volt}.}\label{fig:bias}
\end{figure}

\begin{figure}
    \centering
    \includegraphics[width=0.6\textwidth]{./images/comparator/audio_triangle_wave_biased_100hz.png}
    \caption{Onda triangular de \SI{100}{\hertz} capturada pelo conversor $A_1$
    após aplicar um offset DC de \SI{2,5}{\volt}. A onda varia aproximadamente
    entre \SI{2,0}{\volt} e \SI{3,0}{\volt}.}\label{fig:vI}
\end{figure}

O circuito do comparador é dado pela Figura~\ref{fig:comparator}, onde temos a
entrada de áudio $v_I$ após a aplicação do offset no terminal inversor do
amplificador operacional e uma tensão DC $V_A$ aplicada ao terminal não
inversor, usada para regular o duty cycle do sinal PWM gerado $v_O$. Como
alimentamos o amplificador com uma tensão $V_{CC} = \SI{5}{\volt}$, pelo
datasheet~\cite{amp-datasheet}, a tensão de pico do sinal PWM deve ser $V_{CC} -
\SI{1,5}{\volt} = \SI{3,5}{\volt}$

\begin{figure}
    \centering
    \includegraphics[width=0.6\textwidth]{./images/comparator/comparator.pdf}
    \caption{Circuito do comparador usado para gerar o sinal PWM $v_O$ a partir da
    onda triangular gerada pela saída de áudio do celular com offset DC $v_I$. A
    tensão DC $V_A$ aplicada a entrada não inversora é usada para regular o duty
    cycle do PWM. O comparador é alimentado com uma tensão $V_{CC} =
    \SI{5}{\volt}$}
    \label{fig:comparator}
\end{figure}

\subsection{Divisor resistivo}

Para regular o duty cyle do PWM, devemos variar a tensão DC $V_A$ aplicada no
terminal não inversor. Para isso, usamos o circuito da
Figura~\ref{fig:voltage-divider}, que permite-nos aplicar diferentes tensões
$V_{A_i}$ na entrada.

\begin{figure}
    \centering
    \includegraphics[width=0.6\textwidth]{./images/comparator/voltage-divider.pdf}
    \caption{Projeto do divisor resistivo usado para conseguir aplicar uma
    tensão na entrada não inversora do amplificador de \SI{0}{\volt},
    \SI{0,5}{\volt}, \SI{1,0}{\volt}, \SI{1,5}{\volt}, \SI{2,5}{\volt} e
    \SI{3,0}{\volt}.}
    \label{fig:voltage-divider}
\end{figure}

\subsection{Duty Cycle}

Usando as diferentes tensões $V_{A_i}$ do divisor resistivo projetado na
Figura~\ref{fig:voltage-divider}, foi possível montar a
Tabela~\ref{tab:duty-cycle}, obtida através da medida do duty cycle para cada
uma das possíveis tensões $V_A$. A Figura~\ref{fig:vA} mostra as medidas feitas
pelo oscilloscópio para obter os resultados da Tabela~\ref{tab:duty-cycle}. Note
como para as tensões $V_A \le \SI{2,0}{\volt}$ o duty cycle é de 0\%, já que a
tensão no terminal não inversor é sempre menor a tensão mínima da onda
triangular em $v_I$. Analogamente, para $V_A \ge \SI{3,0}{\volt}$, o duty cycle
é sempre 100\%. Para $V_A = \SI{2,5}{\volt}$, o semiciclo positivo da onda está
acima da tensão no terminal não inversor, enquanto o semiciclo negativo está
abaixo, resultando em um duty cycle de 50\%, como pode ser visto na
Figura~\ref{fig:vA5}.

\begin{table}
\centering
\begin{tabular}{|c|c|}
  \hline
  $V_a$ [\si{\volt}] & DC (\%) \\
  \hline
  0,0 & 0 \\
  1,5 & 0 \\
  1,0 & 0 \\
  1,5 & 0 \\
  2,5 & 50 \\
  3,0 & 100 \\
  \hline
\end{tabular}
  \caption{Duty cycle do sinal PWM gerado a partir de diferentes tensões DC
  $V_A$ aplicadas à entrada não inversora do comparador. Os valores foram
  obtidos a partir das medidas feitas com o oscilloscópio dadas pela
  Figura~\ref{fig:vA}.}
\label{tab:duty-cycle}
\end{table}

\begin{figure}
  \centering
  \begin{subfigure}{0.40\textwidth}
    \includegraphics[width=\textwidth]{./images/comparator/audio_triangle_wave_pwm_0-0v.png}
    \caption{$V_{A_1} = \SI{0,0}{\volt}$}
  \end{subfigure}
  \begin{subfigure}{0.40\textwidth}
    \includegraphics[width=\textwidth]{./images/comparator/audio_triangle_wave_pwm_0-5v.png}
    \caption{$V_{A_2} = \SI{0,5}{\volt}$}
  \end{subfigure} \\ \vspace{0.5em}
  \begin{subfigure}{0.40\textwidth}
    \includegraphics[width=\textwidth]{./images/comparator/audio_triangle_wave_pwm_1-0v.png}
    \caption{$V_{A_3} = \SI{1,0}{\volt}$}
  \end{subfigure}
  \begin{subfigure}{0.40\textwidth}
    \includegraphics[width=\textwidth]{./images/comparator/audio_triangle_wave_pwm_1-5v.png}
    \caption{$V_{A_4} = \SI{1,5}{\volt}$}
  \end{subfigure} \\ \vspace{0.5em}
  \begin{subfigure}{0.40\textwidth}
    \includegraphics[width=\textwidth]{./images/comparator/audio_triangle_wave_pwm_2-5v.png}
    \caption{$V_{A_5} = \SI{2,5}{\volt}$}
    \label{fig:vA5}
  \end{subfigure}
  \begin{subfigure}{0.40\textwidth}
    \includegraphics[width=\textwidth]{./images/comparator/audio_triangle_wave_pwm_3-0v.png}
    \caption{$V_{A_6} = \SI{3,0}{\volt}$}
  \end{subfigure} \\
    \caption{Medidas feitas com o oscilloscópio para observar o duty cycle para
    as diferentes tensões $V_A$ aplicadas na entrada não inversora. A onda
    vermelha é a entrada $v_I$ do terminal inversor do amplificador. A onda azul
    é o valor da tensão $V_A$ aplicado no terminal não inversor. A onda verde é
    a saída $v_O$ do sinal PWM.}
  \label{fig:vA}
\end{figure}
