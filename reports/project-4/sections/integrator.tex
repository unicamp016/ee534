\section{Integrador}\label{sec:integrator}

O circuito integrador é mostrado na Figura~\ref{fig:integrator}. Seguindo a
sugestão do enunciado e mudando apenas o valor de $R_3$, adotamos que $R_3 =
\SI{100}{\kilo\ohm}$, $R_4 = \SI{1}{\mega\ohm}$, $C = \SI{10}{\nano\farad}$ e
aplicamos em $v_i$ uma onda quadrada de \SI{100}{\hertz} e $V_{pp} =
\SI{0.8}{\volt}$ centrada em 0. O amplificador foi alimentado com \SI{5}{\volt}
e \SI{-1.5}{\volt} nos terminais $V_+$ e $V_-$, respectivamente.

\begin{figure}
  \center
  \scalebox{1.0}{
  \begin{circuitikz}
    \draw (0, 0) node[op amp] (opamp) {};
    \draw (opamp.+) node[label=below:$v^+$] {} -- ++(-0.5, 0.0) -- ++(0.0, -0.5) node[circ] (gr) {} -- ++(0.0, -0.1) node[ground] {};
    \draw (gr.center) -- ++(-2.5, 0.0) to[sV, l=$v_i$] ++(0.0, 3.0);
    \draw (opamp.-) node[label=$v^-$] {} -- ++(-0.5, 0.0) -- ++(0.0, 1.5) node[circ] (Rcommon) {} to[R, l_=$R_3$] ++(-2.5, 0.0);
    \draw (Rcommon) to[R, l=$R_4$] ++(2.9, 0.0) node (Rright) {};
    \draw (Rcommon) -- ++(0.0, 1.5) to[C, l=$C$] ++(2.9, 0.0) -- ++(0.0, -1.5) node[circ] {};
    \draw (opamp.out) node[ocirc, label=right:$v_O$] {} -- (Rright.center);
    \draw (opamp.up) -- ++(0.0, 0.5) node[ocirc, label=$V_+$] {};
    \draw (opamp.down) -- ++(0.0, -0.5) node[ocirc, label=below:$V_-$] {};
  \end{circuitikz}
  }
  \caption{Circuito integrador.}
  \label{fig:integrator}
\end{figure}


Como estamos integrando uma onda quadrada, esperamos uma onda triangular na
saída.  A tensão $v_i$ varia entre \SI{-0.4}{\volt} e \SI{0.4}{\volt}. No
semiciclo positivo, esperamos que uma corrente ${i_s =
\frac{\SI{0.4}{\volt}}{\SI{100}{\kilo\ohm}} = \SI{4}{\micro\ampere}}$ passe
pelo resistor $R_3$. Aproximando a impedância de $R_3$ por $\infty$, podemos supor,
para efeito da estimativa, $i_C = i_s$ e toda corrente passa pelo capacitor.

Sabendo que o gradiente da tensão que passa pelo capacitor vale $\diff{V}{t} =
\frac{i}{C}$ e o amplificador está na configuração inversora, esperamos que a
inclinação da onda triangular na saída para o semiciclo positivo da tensão
de entrada seja de
$\SI{-0.4}{\volt\per\milli\second}$. A mesma análise vale para o semiciclo
negativo da onda, onde esperamos que a inclinação da tensão na saída seja
$\SI{0.4}{\volt\per\milli\second}$.

%A corrente no capacitor é dada pelo divisor de corrente
%entre o capacitor $C$ e o resistor $R_4$, de onde temos que a corrente $i_C$
%que passa por ele vale $i_C = \frac{R_4}{R_4 + Z_C} i_s$, onde $Z_C$ é a
%impedância do capacitor. Dessa forma, temos que $i_C = i_s
%\frac{\SI{1}{\mega\ohm}}{\SI{1}{\mega\ohm} + (2\pi \cdot \SI{100}{\hertz} \cdot
%\SI{10}{\nano\farad})^{-1}} \approx \SI{1.725}{\micro\ampere}$.

A Figura~\ref{fig:integrator-scope} mostra as medidas feitas da tensão de entrada
$v_i$, tensão no terminal negativo $v^-$ e na saída do amplificador $v_O$.
Podemos ver que durante o semiciclo positivo da onda quadrada aplicada na
entrada, a saída $v_O$ decai linearmente e no semiciclo negativo, ela cresce
linearmente, formando uma onda triangular simétrica, já que os períodos de ambos
os semiciclos são iguais. A inclinação da onda triangular na
saída pode ser calculada a partir da tensão de pico e sabendo que a
onda demora $\nicefrac{T}{4} = \SI{2.5}{\milli\second}$ para decair do pico até
\SI{0}{\volt}. Dessa forma, para o semiciclo positivo da entrada, calculamos que
$\diff{V}{t} = -\frac{\SI{1.08}{\volt}}{\SI{2.5}{\milli\second}} =
\SI{-432}{\milli\volt\per\milli\second}$, que está muito próximo dos
\SI{-400}{\milli\volt\per\milli\second} esperados. Assumindo que a maior
parte do erro vem da aproximação usada para a corrente $i_C$ que passa pelo
capacitor, podemos afirmar que $v_O$ comporta-se como esperado.

\begin{figure}
  \center
  \includegraphics[width=0.5\textwidth]{images/integrator/integrator.png}
  \caption{Medidas da tensão de saída $v_O$ (vermelho), tensão no terminal
  negativo $v^-$ (verde) e sinal de entrada $v_i$ (azul) feitas com o
  osciloscópio para o circuito integrador da Figura~\ref{fig:integrator}. A
  tensão de pico da saída vale $v_{O_p} = \SI{1.080}{\volt}$.}
  \label{fig:integrator-scope}
\end{figure}

O resistor $R_4$ é essencial para um circuito prático de um integrador montado
com um amplificador operacional. Na prática, os amplificadores têm uma corrente
de repouso (\emph{bias}), que está presente mesmo quando os dois terminais da
entrada estão aterrados. Para o LM324, a corrente de repouso vale
\SI{45}{\nano\ampere}~\cite{amp-datasheet}, que iria passar pelo capacitor,
carregando ele e fazendo com que a tensão de saída aumentasse com o passar do
tempo até o amplificador saturar. Ao adicionar um resistor grande em paralelo
com o capacitor, a corrente de repouso irá passar pelo resistor, que tem uma
impedância finita e menor que a do capacitor ($\infty$), impedindo com que a
corrente passe por ele carregando-o ao longo do tempo. O resistor precisa ser
grande para que a corrente do sinal AC passe majoritariamente pelo capacitor.
