\section{Comparador Schmitt}\label{sec:bistable}

Para o circuito do multivibrador biestável (\ie comparador Schmitt) dado pela
Figura~\ref{fig:bistable}, projetamos o range da janela de histerese de forma
que esteja contido no range da tensão de entrada vinda da saída de áudio $v_i
\in [\SI{-0.4}{\volt}, \SI{0.4}{\volt}]$.  Dessa forma, será possível observar
a tensão de saída oscilando entre os limites de saturação do amplificador
operacional.

Usando a fonte do arduino para a alimentação positiva e uma pilha para a
alimentação negativa, temos que $V_+ = \SI{5}{\volt}$ e $V_- =
\SI{-1.5}{\volt}$. Pelo datasheet do LM324~\cite{amp-datasheet}, sabemos que os
limiares de saturação valem $L_+ = V_+ - \SI{1.5}{\volt} = \SI{3.5}{\volt}$ e
$L_- = V_- = \SI{-1.5}{\volt}$. O range da janela de histerese $[V_{TL},
V_{TH}]$ pode ser obtido calculando a tensão no terminal $v^+$ para os limites
de saturação $L_-$ e $L_+$, respectivamente. A partir do divisor resistivo,
temos que $\beta = \frac{R_1}{R_1 + R_2}$, e $V_{TL} = \beta L_-$ e $V_{TH} =
\beta L_+$.  Escolhendo $\beta \le \frac{\SI{0.4}{\volt}}{\SI{3.5}{\volt}}
\approx \num{0.114}$, temos que o range da janela de histerese estará contido
em $[-\num{171}, \num{400}]\si{\milli\volt}$, que, por sua vez, está contido no
range de $v_i$.  Portanto, no circuito da Figura~\ref{fig:bistable}, escolhemos
$R_1 = \SI{1}{\kilo\ohm}$ e $R_2 = \SI{8.2}{\kilo\ohm}$, de forma que $\beta
\approx \num{0.109}$ e a janela de histerese está entre $[\num{-163},
\num{380}]\si{\milli\volt}$. Se aplicarmos uma onda triangular na entrada $v_i$,
podemos estimar o duty cycle que será obtido com o multivibrador biestável.
A tensão no terminal negativo irá variar linearmente entre \SI{0}{\volt},
\SI{0.4}{\volt}, \SI{-0.4}{\volt} e de volta para \SI{0}{\volt}, percorrendo
\SI{1.6}{\volt} até completar um período. Se assumirmos que $v^+$ vale $\beta
L_+ \approx \SI{380}{\milli\volt}$, e que o terminal $v^-$ acabou de
ultrapassar $v^+$, mudando o estado da saída para $L_-$ e $v^+$ para $\beta L_-
\approx \SI{-163}{\milli\volt}$. A saída $v_O$ permanecerá no limiar inferior
de saturação até que a tensão no terminal $v^-$ seja inferior ao novo valor de
$v^+ = \SI{-163}{\milli\volt}$. Dessa forma, temos que o \emph{duty cycle}
esperado da saída vale $\SI{100}{\percent} \cdot (1 - \frac{[(\num{400} - \num{380}) +
(\num{400} + \num{163})]\si{\milli\volt}}{\SI{1.6}{\volt}}) \approx
\SI{63.6}{\percent}$.

\begin{figure}[h]
  \center
  \begin{circuitikz}
    \draw (0, 0) node[op amp, yscale=-1] (opamp) {};
    \draw (opamp.-) node[label=$v^-$] {} -- ++(-1.0, 0.0) to[sV, l=$v_i$] ++(0.0, -1.5) -- ++(0.0, -0.0) node[ground] {};
    \draw (opamp.+) node[label=$v^+$] {} -- ++(-0.5, 0.0) -- ++(0.0, 1.5) node[circ] (Rcommon) {} to[R, l_=$R_1$] ++(-2.5, 0.0) -- ++(0.0, -1.0) node[ground]{};
    \draw (Rcommon) to[R, l=$R_2$] ++(2.9, 0.0) node (Rright) {};
    \draw (opamp.out) node[ocirc, label=right:$v_O$] {} -- (Rright.center);
    \draw (opamp.down) -- ++(0.0, 0.5) node[ocirc, label=$V_+$] {};
    \draw (opamp.up) -- ++(0.0, -0.5) node[ocirc, label=below:$V_-$] {};
  \end{circuitikz}
  \caption{Circuito do multivibrador biestável}
  \label{fig:bistable}
\end{figure}

Na Figura~\ref{fig:bistable-no-offset}, podemos ver que a tensão de saída
comporta-se como esperado. Quando a tensão no terminal positivo $v^+$ (azul) é
maior que a tensão de entrada $v_i$ (verde), a tensão de saída satura em
\SI{3.533}{\volt}. No restante do tempo, ela satura no limiar
negativo. O \emph{duty cycle} medido foi de \SI{70}{\percent},
que é um pouco maior do que o valor esperado de \SI{63.6}{\percent}. Note que o
erro da medida do duty cyle é de $\pm \SI{2.5}{\percent}$, já que só coletamos
20 amostras por período.

Na Figura~\ref{fig:bistable-offset}, deslocamos a tensão
de saída e de entrada em aproximadamente \SI{2.5}{\volt}, de forma que foi possível
medir a limiar de saturação negativo que vale $L_- = \SI{-729}{\milli\volt}$ e é
bem menor do que esperávamos. A informação sobre o comportamento da saturação
para o LM324 com alimentação negativa não foi encontrada no datasheet e por
isso foi feita a suposição nos cálculos que $V_- = L_-$. Essa diferença
significativa justifica o erro no cálculo do \emph{duty cycle}, que valeria
\SI{28.9}{\percent} se usássemos o valor medido de \SI{-729}{\milli\volt} para
$L_-$. Dessa forma, o \emph{duty cycle} de \SI{30}{\percent} está condizente
com o valor medido para $L_-$, já que a margem de erro contempla o valor
esperado de \SI{28.9}{\percent}.

\begin{figure}[h]
  \center
  \begin{subfigure}{0.49\textwidth}
    \includegraphics[width=\textwidth]{images/bistable/bistable-2.png}
    \caption{$V_\text{off} = \SI{0}{\volt}$}
    \label{fig:bistable-no-offset}
  \end{subfigure}
  \begin{subfigure}{0.49\textwidth}
    \includegraphics[width=\textwidth]{images/bistable/bistable-offset.png}
    \caption{$V_\text{off} = \SI{2.5}{\volt}$}
    \label{fig:bistable-offset}
  \end{subfigure}
  \caption{Medidas da tensão de saída $v_O$ (vermelho), tensão no terminal
  negativo $v^- = v_i$ (verde) e no terminal positivo $v^+$ (azul) para o
  multivibrador biestável. A tensão de pico dos terminais $v_O$, $v^+$ e $v^-$
  valem \SI{3.533}{\volt}, \SI{386}{\milli\volt} e \SI{400}{\milli\volt},
  respectivamente. O duty cyle da tensão de saída vale \SI{70}{\percent}, visto
  que das 20 amostras tomadas por período, 14 delas valem $v_O =
  \SI{3.533}{\volt}$. À esquerda temos a medida feita sem aplicar um
  \emph{offset} na entrada do conversor A/D, enquanto à direita
  aplicou-se um offset de aproximadamente \SI{2.5}{\volt}. Medimos que
  $v_{O_{pp}} = \SI{4.262}{\volt}$ e que $L_- = \SI{-729}{\milli\volt}.$}
  \label{fig:bistable-scope}
\end{figure}

A Figura~\ref{fig:bistable-hysteresis} mostra a relação de $v_O \times v_i$
para biestável com e sem \emph{offset}. Na
Figura~\ref{fig:bistable-hysteresis-no-offset}, a gente pode ver a relação real
do circuito, onde a saída vale \SI{3.533}{\volt} até que a entrada ultrapasse
\SI{383}{\milli\volt}. A Figura~\ref{fig:bistable-hysteresis-offset} mostra a
janela completa de histerese, onde podemos observar uma largura de
aproximadamente \SI{450}{\milli\volt}, como esperado, e a saída oscilando entre
o limiar positivo e negativo de saturação.

\begin{figure}[h]
  \center
  \begin{subfigure}{0.49\textwidth}
    \includegraphics[width=\textwidth]{images/bistable/bistable-vin-vout.png}
    \caption{$V_\text{off} = \SI{0}{\volt}$}
    \label{fig:bistable-hysteresis-no-offset}
  \end{subfigure}
  \begin{subfigure}{0.49\textwidth}
    \includegraphics[width=\textwidth]{images/bistable/bistable-vin-vout-offset.png}
    \caption{$V_\text{off} = \SI{2.5}{\volt}$}
    \label{fig:bistable-hysteresis-offset}
  \end{subfigure}
  \caption{Medida da relação $v_O \times v_i$ (magenta) para o circuito do
  multivibrador biestável da Figura~\ref{fig:bistable} onde aplicamos um
  \emph{offset} de \SI{0}{\volt} (esquerda) e \SI{2.5}{\volt} na entrada do
  conversor A/D do arduino. O range da janela de histerse vale cerca de
  \SI{450}{\milli\volt}.}
  \label{fig:bistable-hysteresis}
\end{figure}
