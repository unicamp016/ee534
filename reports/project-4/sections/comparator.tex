\section{Comparador}\label{sec:comparator}
Este experimento envolve o uso de um comparador para gerar um sinal de PWM.
Vamos usar um amplificador do CI LM324~\cite{amp-datasheet} em malha aberta e
regular o DC (\emph{duty cycle}) do sinal PWM através da tensão na saída não
inversora do amplificador.

\subsection{Montagem}
Uma onda senoidal de \SI{100}{\hertz} foi gerada a partir do celular usando o
aplicativo ``Frequency Generator''~\cite{freqgen}. Usando um cabo de aúdio P2,
foi possível medir o sinal da saída de áudio do celular $v_i$ com o
oscilloscópio DIY\@. Como o sinal oscila em torno do terra, foi necessário
aplicar um offset DC para medir o excursão toda do sinal. A Figura~\ref{fig:offset}
mostra o circuito usado para aplicar um offset de \SI{0}{\volt} a \SI{5}{\volt}
através do ajuste do potenciômetro. O sinal de saída do circuito $v_I$ é dado
pela Figura~\ref{fig:vi}.

\begin{figure}
  \center
  \scalebox{0.8}{
  \begin{circuitikz}
    \draw (0, 0) to[sV, l=$v_i$] ++(0, 1.7) -- ++(1.8, 0.0) node (Rup) {};
    \draw (0, 0) -- (0.0, -0.2) node[ground] {};
    \draw (Rup) to[R, l=$\SI{10}{\kilo\ohm}$] ++(0.0, -1.7) -- ++(0.0, -0.2) node[ground] {};
    \draw (Rup) to[pC, l=$\SI{47}{\micro\farad}$] ++ (2.0, 0.0) node (Cright) {};
    \draw (Cright.center) -- ++(1.5, 0.0) node[ocirc, label=$v_I$] {};
    \draw (Cright) to[R, l=$\SI{10}{\kilo\ohm}$] ++(0.0, -1.7) -- ++(0.0, -2.75) node (Rdown) {};

    \draw (1.3, -1.5) node (pRup) {} to[pR, n=POT, l_=$\SI{10}{\kilo\ohm}$] ++(0.0, -2.5) node[ground] (pRdown) {};
    \draw (Rdown.center) -- ++(-1.0, 0.0) node (Cup) {} -- (POT.wiper);
    \draw (pRup.center) -- ++(-2.0, 0.0) to[battery1, l_=$V_{CC}$] ++(0.0, -2.0) node[ground] {};
    \draw (pRdown.center) -- ++(1.5, 0.0) to[pC, l_=$\SI{47}{\micro\farad}$] (Cup.center);
  \end{circuitikz}
  }
  \caption{Circuito usado para aplicar um offset na entrada analógica $v_i$
  vinda da saída de áudio do celular e produzir uma saída $v_I$. Alimentamos o
  circuito com $V_{CC} = \SI{5}{\volt}$ e usamos um potenciômetro de 20 voltas,
  de forma que o offset aplicado pode ser de \SI{0}{\volt} a \SI{5}{\volt} em
  incrementos de $\frac{\SI{5}{\volt}}{20} = \SI{0.25}{\volt}$.}
  \label{fig:offset}
\end{figure}

\begin{figure}
  \center
  \includegraphics[width=0.5\textwidth]{images/comparator/v_i.png}
  \caption{Sinal de entrada $v_I$ após aplicar um offset DC de \SI{1.0}{\volt}
  na entrada $v_i$ do celular. O sinal gerado pelo celular é uma onda senoidal
  com frequência de \SI{100}{\hertz}. O offset DC foi aplicado usando ajustando
  o potenciômetro no circuito da Figura~\ref{fig:offset}. Usando o
  osciloscópio, mediu-se que $v_{I_{pp}} \approx \SI{0.8}{\volt}$.}
  \label{fig:vi}
\end{figure}

Para montar o PWM, vamos usar um amplificador operacional como comparador,
usando o circuito da Figura~\ref{fig:comparator}. A tensão
$v_I$ do circuito é dada pela Figura~\ref{fig:vi}, enquanto a tensão $V_{DC}$
foi obtida através do divisor resistivo da Figura~\ref{fig:divider}, onde
os nós $V_{A_i}$ são os diferentes valores de $V_{DC}$ aplicados ao terminal
positivo do amplificador.

\begin{figure}[h]
  \center
  \scalebox{0.8}{
  \begin{circuitikz}
    \draw (0, 0) node[op amp, yscale=-1] (opamp) {};
    \draw (opamp.+) -- (-3, 0.5);
    \draw (-3, 0.5) to[battery1, l_=$V_{DC}$] (-3,-1.5) -- (-3,-1.7) node[ground]{};
    \draw (opamp.-) -- (-1.7, -0.5);
    \draw (-1.7, -0.5)  to[sV, l_=$v_I$] (-1.7, -1.5) -- (-1.7, -1.7) node[ground]{};
    \draw (opamp.down) -- ++(0.0, 0.5) node[ocirc, label=$V_+$] {};
    \draw (opamp.up) -- ++(0.0, -0.5) node[ocirc, label=below:$V_-$] {};
    \draw (opamp.out) -- ++(0.5, 0.0) node[ocirc, label=below:$v_O$] {};
  \end{circuitikz}
  }
  \caption{Comparador em malha aberta com ajuste do nível de chaveamento.}
  \label{fig:comparator}
\end{figure}


\begin{figure}[h]
  \center
  \scalebox{0.8}{
  \begin{circuitikz}
    \draw (0, 0) -- ++(0, 0.0) node[ground] {};
    \draw (0, 1.5) node (Vup) {} to[battery1, l_=$V_{CC}$] ++(0.0, -1.5);
    \draw (Vup.center)
      to[R, l=\SI{5}{\kilo\ohm}] ++(2.0, 0.0)
      to[R, l=\SI{2}{\kilo\ohm}] ++(2.0, 0.0) node[circ, label=$V_{A_3}$] {}
      to[R, l=\SI{1}{\kilo\ohm}] ++(2.0, 0.0) node[circ, label=right:$V_{A_2}$] {}
      to[R, l=\SI{1}{\kilo\ohm}] ++(0.0, -2.0) node[circ, label=right:$V_{A_1}$] {}
      to[R, l=\SI{1}{\kilo\ohm}] ++(0.0, -2.0)
      -- ++ (0.0, 0.0) node[ground] {};
  \end{circuitikz}
  }
  \caption{Divisor resistivo projetado para aplicar tensão DC na entrada
  não-inversora $V_{A_i}$ de \SI{0.5}{\volt}, \SI{1.0}{\volt} e
  \SI{1.5}{\volt}}.
  \label{fig:divider}
\end{figure}

Para usar toda a excursão do conversor A/D do arduino, alimentamos o terminal
positivo do amplificador $V_+$ com \SI{6.5}{\volt}, usando uma pilha de
\SI{1.5}{\volt} em série com uma fonte de \SI{5}{\volt}. O terminal negativo
$V_-$ foi conectado ao terra. Pelo datasheet do LM324~\cite{amp-datasheet}, o
amplificador satura em $L_+ = V_+ - \SI{1.5}{\volt}$ e $L_- = V_-$. Dessa
forma, temos que $v_O$ irá variar entre $L_+ = \SI{5}{\volt}$ e $L_- =
\SI{0}{\volt}$.

\subsection{Duty Cycle}

A Figura~\ref{fig:vA} mostra o sinal de saída $v_O$ e a relação $v_I \times
v_O$ em função das diferentes tensões $V_{A_i}$ aplicadas ao terminal positivo
do amplificador. Note como para $V_{A_1}$ na Figura~\ref{fig:vA-05}, a saída é
\SI{0}{\volt}, já que a tensão $V_{DC} < v_I(t)$ durante todo período da onda,
de forma que o \emph{duty cycle} é de 0\%. A relação $v_I \times v_O$ reflete
esse comportamento, onde vemos que $v_O$ é constante e vale \SI{0}{\volt} para
todo valor de $v_I$.

Para $V_{A_2}$ na Figura~\ref{fig:vA-10}, a tensão $V_{DC}$ está bem no centro
de $v_I$, de forma que durante metade do período $v_I(t) < V_{DC}$ e durante
outra metade $v_I(t) > V_{DC}$. Portanto, podemos observar um \emph{duty cycle}
de 50\% na saída. Na medida da relação $v_I \times v_O$, temos que para $v_I <
V_{DC}$, a tensão de saída $v_O = L_+ = \SI{5}{\volt}$ e para $v_I > V_{DC}$,
$v_O = L_- = \SI{0}{\volt}$. A reta quase horizontal se deve ao fato que o
osciloscópio interpola os pontos medidos para obter uma curva contínua.

Por fim, para $V_{A_3}$ na Figura~\ref{fig:vA-15}, a tensão $V_{DC} > v_I(t)$
durante todo período, de modo que o sinal PWM tem o \emph{duty cycle} de 100\%.
No gráfico da relação $v_I \times v_O$, podemos ver pelo traço em magenta que
$v_O = \SI{5}{\volt}$ para qualquer valor de $v_I$.

Visto que o limiar positivo de saturação $L_+$ está bem no limite do range de
tensão do conversor A/D do arduino, não é possível saber por certo a tensão
$L_+$ usando o osciloscópio. Portanto, realizamos uma medida com o multímetro,
com o qual obtivemos que $L_+ = \SI{5.02}{\volt}$, e está próximo do valor
esperado de acordo com o datasheet.

\begin{figure}
  \centering
  \begin{subfigure}{\textwidth}
    \center
    \begin{subfigure}{0.40\textwidth}
      \includegraphics[width=\textwidth]{./images/comparator/comparator-0-5V.png}
    \end{subfigure}
    \begin{subfigure}{0.40\textwidth}
      \includegraphics[width=\textwidth]{./images/comparator/comparator-vinxvout-0-5V.png}
    \end{subfigure} \\
    \caption{$V_{A_1} = \SI{0.5}{\volt}$}
    \label{fig:vA-05}
  \end{subfigure} \\ \vspace{0.5em}
  \begin{subfigure}{\textwidth}
    \center
    \begin{subfigure}{0.40\textwidth}
      \includegraphics[width=\textwidth]{./images/comparator/comparator-1-0V.png}
    \end{subfigure}
    \begin{subfigure}{0.40\textwidth}
      \includegraphics[width=\textwidth]{./images/comparator/comparator-vinxvout-1-0V.png}
    \end{subfigure} \\
    \caption{$V_{A_2} = \SI{1.0}{\volt}$}
    \label{fig:vA-10}
  \end{subfigure} \\ \vspace{0.5em}
  \begin{subfigure}{\textwidth}
    \center
    \begin{subfigure}{0.40\textwidth}
      \includegraphics[width=\textwidth]{./images/comparator/comparator-1-5V.png}
    \end{subfigure}
    \begin{subfigure}{0.40\textwidth}
      \includegraphics[width=\textwidth]{./images/comparator/comparator-vinxvout-1-5V.png}
    \end{subfigure} \\
    \caption{$V_{A_3} = \SI{1.5}{\volt}$}
    \label{fig:vA-15}
  \end{subfigure} \\ \vspace{0.5em}
    \caption{À esquerda, temos as medidas da tensão de entrada $v_I$ (verde),
    de saída $v_O$ (vermelho) e da tensão $V_{DC}$ (azul) para cada um dos
    valores de $V_{A_i}$ aplicados ao terminal não inversor do amplificador. À
    direita, temos a relação $v_I \times v_O$ (magenta) e a saída $v_O$ para os
    cada valor de $V_{A_i}$.}
  \label{fig:vA}
\end{figure}
