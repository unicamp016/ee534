\section{Multivibrador Astável}\label{sec:astable}

Nesta seção vamos projetar um multivibrador astável, que usa um circuito $RC$ no
lugar da tensão de entrada $v_i$ do biestável. O diagrama do circuito é dado
pela Figura~\ref{fig:astable}. Para deduzir a expressão do período da tensão
de saída, devemos primeiro entender como o capacitor se carrega e descarrega.
Dada uma tensão inicial $V_{0+}$ e uma tensão final $V_{\infty}$, temos que a tensão
no capacitor $C$ é

\begin{equation}\label{eq:capv}
  v(t) = V_{\infty} - (V_{\infty} - V_{0+}) e^{-t/\tau},
\end{equation}

onde $\tau = CR$ é a constante de tempo do capacitor.

Como visto na Seção~\ref{sec:bistable}, a saída $v_O$ sempre valerá um dos
limites de saturação $L_-$ ou $L_+$. Digamos que a saída vale $L_+$. Nesse
caso, o capacitor $C$ vai se carregar até esse nível através do resistor $R_C$.
Dessa forma, a tensão na saída através do capacitor, que é aplicada no terminal
$v^-$ do amplificador, irá aumentar exponencialmente até atingir $L_+$ com a
constante de tempo $\tau$.  Enquanto isso, o terminal positivo $v^+$ vai
permanecer em $\beta L_+$, até que o capacitor se carregue o suficiente para
ultrapassar $v^+$, momento em que o biestável irá trocar para o limiar negativo
$L_-$. No limiar negativo, o mesmo fenômeno acontece, o capacitor irá se
descarregar do nível de tensão $\beta L_+$ até atingir $L_-$.  Ao chegar em
$\beta L_-$, o biestável irá mudar de estado novamente e um período estará
completo.

Dessa forma, podemos calcular o tempo $T_1$ para o capacitor se carregar de
$\beta L_-$ até $\beta L_+$ usando a Equação~\ref{eq:capv}, de onde temos que

\begin{align*}\label{eq:t1}
  \beta L_+ &= L_+ - (L_+ - \beta L_-) e^{-T_1/\tau}, \\
  e^{-T_1/\tau} &= \frac{L_+ (1 - \beta)}{L_+ - \beta L_-}, \\
  e^{-T_1/\tau} &= \frac{1 - \beta}{1 - \beta (L_-/L_+)}, \\
  T_1 &= \tau \ln \frac{1 - \beta (L_-/L_+)}{1 - \beta}. \\
\end{align*}

De forma análoga, podemos calcular o tempo $T_2$ para o capacitor se descarregar
de $\beta L_+$ até $\beta L_-$ como

\begin{equation*}
  T_2 = \tau \ln \frac{1 - \beta(L_+ / L_-)}{1 - \beta}.
\end{equation*}

Para produzir um sinal de saída com frequência $f = \SI{100}{\hertz}$, devemos
ter que $T = T_1 + T_2 = 1/f = \SI{10}{\milli\second}$. Se fixarmos a capacitância
$C = \SI{100}{\nano\farad}$, podemos encontrar o valor de $R_C$ pela seguinte relação

\begin{equation}
  R_C^{-1} = \frac{C}{T} \left(\ln \frac{1 - \beta (L_-/L_+)}{1 - \beta} + \ln \frac{1 - \beta(L_+ / L_-)}{1 - \beta}\right).
\end{equation}

Substituindo os valores de $\beta$, $L_-$ e $L_+$ da
Seção~\ref{sec:comparator}, obtemos que $R_C = \SI{146}{\kilo\ohm}$. Usando um
resistor de \SI{100}{\kilo\ohm} e um potenciômetro de \SI{100}{\kilo\ohm} de 20
voltas, aplicamos uma resistência de \SI{145}{\kilo\ohm} no lugar de $R_C$, com
a qual esperamos obter um sinal de saída com frequência de \SI{100.6}{\hertz},
um período de carregamento $T_1$ de \SI{1.98}{\milli\second} e um
período de descarregamento $T_2$ de \SI{7.96}{\milli\second}.

\begin{figure}
  \center
  \scalebox{0.8}{
  \begin{circuitikz}
    \draw (0, 0) node[op amp, yscale=-1] (opamp) {};
    \draw (opamp.-) node[label=below:$v^-$] {} -- ++(-1.0, 0.0) -- ++(0.0, -1.5) node[circ] (Cup) {} to[C, l_=$C$] ++(0.0, -1.0) -- ++(0.0, 0.0) node[ground] {};
    \draw (opamp.+) node[label=$v^+$] {} -- ++(-0.5, 0.0) -- ++(0.0, 1.5) node[circ] (Rcommon) {} to[R, l_=$R_1$] ++(-2.5, 0.0) -- ++(0.0, -1.0) node[ground]{};
    \draw (Rcommon) to[R, l=$R_2$] ++(2.9, 0.0) node (Rright) {};
    \draw (opamp.out) node[ocirc, label=right:$v_O$] (ampout) {} -- (Rright.center);
    \draw (opamp.down) -- ++(0.0, 0.5) node[ocirc, label=$V_+$] {};
    \draw (opamp.up) -- ++(0.0, -0.5) node[ocirc, label=below:$V_-$] {};
    \draw (ampout) -- ++(0.0, -2.0) to[R, l=$R_C$] (Cup);
  \end{circuitikz}
  }
  \caption{Multivibrador astável.}
  \label{fig:astable}
\end{figure}

A Figura~\ref{fig:astable-scope} mostra a medida do sinal de saída $v_O$, do
terminal negativo $v^-$ e positivo $v^+$. Como projetado, a frequência do sinal
de saída vale \SI{100}{\hertz}. Porém, devemos levar em conta que o
osciloscópio tem uma margem de erro alta nas medidas de frequência, visto
estamos tomando uma amostra por millisegundo e um erro de 1 amostra a mais ou a
menos muda a frequência do sinal de \SI{100}{\hertz} para \SI{90.9}{\hertz},
por exemplo. Pela figura, podemos ver que o capacitor passa \SI{2}{\milli\second}
se carregando e \SI{8}{\milli\second} se descarregando, resultando em um
\emph{duty cycle} de \SI{20}{\percent}, que está contido na margem de erro do
osciloscópio. No terminal positivo da entrada $v^+$, podemos ver o valor
para o limiar positivo da janela de histerese do multivibrador, que vale
\SI{386}{\milli\volt}. No terminal negativo $v^-$, pode-se ver como o capacitor
se carrega exponencialmente até atingir o limiar positivo da janela de
histerese, a partir de onde ele passa a se descarregar exponencialmente até
atingir o limiar negativo.

\begin{figure}
  \center
  \includegraphics[width=0.5\textwidth]{images/astable/astable.png}
  \caption{Medida da tensão de saída $v_O$ (vermelho), terminal negativo $v^-$
  (verde) e terminal positivo $v^+$ (azul) para o multivibrador astável da
  Figura~\ref{fig:astable}. Mediu-se uma frequência de \SI{100}{\hertz} para
  a tensão de saída e um \emph{duty cycle} de \SI{20}{\percent}, visto que
  das 10 amostras feitas por período, 2 delas valem $L_+$.}
  \label{fig:astable-scope}
\end{figure}
