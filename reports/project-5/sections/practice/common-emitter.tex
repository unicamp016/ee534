\subsection{Emissor Comum}\label{sec:practice-ce}

Para a configuração emissor-comum, usamos o circuito da Figura~\ref{fig:ce-arduino}, onde introduzimos um capacitor em paralelo com a fonte $V_{CC}$ em relação ao circuito da Figura~\ref{fig:ce} para eliminar o ruído AC da fonte de \SI{5}{\volt} do arduino. No circuito, usamos $C_1 = C_2 = \SI{47}{\micro\farad}$ e medimos com o multímetro que $\RB = \SI{733}{\kilo\ohm}$, $R_C = \SI{1205}{\ohm}$ e $\B = \num{350}$.

\begin{figure}
  \center
  \scalebox{1.0}{
  \begin{circuitikz}[american]
    \draw node[npn, label=right:$Q$] (Q) {};
    \draw (Q.E) -- ++(0, -2.25) -- ++(-1.8, 0) node[ground] {} -- ++(-2, 0) to[sV, l=$v_i$] ++(0, 3) to[C, l=$C_2$] ++(2, 0) node[circ, label=45:$V_B$] (Cright) {};
    \draw (Q.B) -- (Cright.center);
    \draw (Cright.center) -- ++(0, 0.75) to[R, l=$\RB$] ++(0, 2) node (RBup) {} -- ++(1.8, 0) node (RCup) {} to[R, l=$R_{C}$] (Q.C);
    \draw (RBup.center) -- ++ (-2.0, 0) node (Cv) {} -- ++ (-1.5, 0) to[V, l_=$V_{CC}$] ++ (0, -1.5) node[ground] {};
    \draw (Cv.center) to[C, l_=$C_1$] ++ (0, -1.5) node[ground] {};
    \draw (Q.C) -- ++(0.5, 0) node[ocirc, label=right:$V_C$] {};
  \end{circuitikz}
  }
  \caption{Transistor bipolar NPN na configuração emissor-comum.}
  \label{fig:ce-arduino}
\end{figure}

% TODO: falar da fonte vi

\subsubsection{Análise de Grandes Sinais}\label{sec:ls-practice-ce}

Na análise de grandes sinais, removemos a fonte $v_i$ do circuito da Figura~\ref{fig:ce-arduino} e medimos a tensão no coletor, na base e no emissor. Pela Figura~\ref{fig:ce-dc-arduino}, temos que $V_C = \SI{2.497}{\volt}$ e $V_B = \SI{659}{\milli\volt}$. A partir das medidas, podemos calcular que

\begin{align}
    I_C &= \frac{V_{CC} - V_C}{R_C} = \frac{\SI{5}{\volt} - \SI{2.497}{\volt}}{\SI{1205}{\ohm}} \approx \fbox{\SI{2.069}{\milli\ampere}}, \\
    I_B &= \frac{V_{CC} - V_B}{\RB} = \frac{\SI{5}{\volt} - \SI{659}{\milli\volt}}{\SI{733}{\kilo\ohm}} \approx \fbox{\SI{5.922}{\micro\ampere}}, \\
    \B &= \frac{I_C}{I_B} = \frac{\SI{1.708}{\milli\ampere}}{\SI{5.943}{\micro\ampere}} \approx \fbox{\num{349.4}}.
\end{align}

Ao contrário do que observamos nos experimentos em simulação da Seção~\ref{sec:ls-theory-ce}, o valor de \B calculado e a tensão $V_C$ medida são iguais aos valores usados e obtidos no projeto, sinal que o projeto foi bem sucedido em obter um ponto de operação com a máxima excursão no sinal de saída. Como o projeto foi feito usando o valor medido de \B, é natural observarmos um resultado melhor em relação ao resultado em simulação, já que o modelo SPICE usa como \B o valor nominal do datasheet, que vale \num{290}.

\begin{figure}
    \centering
    \includegraphics[width=0.5\textwidth]{images/practice/common-emitter/ce-dc.png}
    \caption{Medidas da tensão na base $V_B$ (verde), no coletor $V_C$ (vermelho) e no emissor $V_E$ (emissor) para o circuito montado da Figura~\ref{fig:ce} na temperatura ambiente. Mediu-se que $V_B = \SI{659}{\milli\volt}$, $V_C = \SI{2.497}{\volt}$ e $V_E = \SI{0}{\volt}$.} 
    \label{fig:ce-dc-arduino}
\end{figure}

\subsubsection{Análise de Pequenos Sinais}\label{sec:ss-practice-ce}

Para a análise de pequenos sinais aplicamos um sinal senoidal $v_i$ com frequência de \SI{100}{\hertz} usando a saída de áudio do celular através do aplicativo ``Frequency Generator''~\cite{freqgen}. No projeto, assumimos que a variação na tensão $v_{be} \ll V_T$ para que possamos usar uma aproximação linear para a relação exponencial entre a corrente $i_c$ no coletor e a tensão entre a base e o emissor $v_{be}$. Na simulação usamos para $v_i$ uma onda senoidal com frequência de \SI{100}{\hertz} e tensão de pico $v_{i_p} = \SI{5}{\milli\volt}$, de forma que a aproximação linear usada ainda fosse válida. Porém, para o experimento no arduino, optamos por usar uma amplitude maior para o sinal $v_i$, visto que a resolução em tensão do conversor AD do arduino vale \SI{4.89}{\milli\volt} e não seria possível observar o ganho do amplificador ou realizar medidas precisas trabalhando nessa ordem de grandeza. Controlamos a amplitude do sinal de entrada usando o volume da saída de áudio do celular. Aplicando um volume de \SI{60}{\percent} através do aplicativo, obtivemos que $v_{i_p} \approx \SI{50}{\milli\volt}$, com o qual realizamos as medidas da Figura~\ref{fig:ce-ac-arduino}. As Figuras~\ref{fig:ce-ac-amb-arduino} e~\ref{fig:ce-ac-hot-arduino}, mostram o sinal de saída no coletor $v_C$, na base $v_B$ e o sinal de entrada $v_i$ para o transistor nas temperaturas ambiente e após aproximar o ferro de solda para aquecê-lo, respectivamente.

Usando a Figura~\ref{fig:ce-ac-amb-arduino}, mediu-se que o ganho na temperatura ambiente vale $G_v(T_a) = \num{-47.929}$, um valor \SI{50.1}{\percent} menor que o ganho de \num{-96.1} estimado no projeto teórico. Como nos experimentos em simulação usamos uma tensão $v_i$ da ordem de grandeza apropriada para a aproximação linear, fazia sentido comparar o ganho medido com o esperado. Porém, já que violamos essa aproximação para os experimentos com arduino, não faz sentido comparar o ganho teórico com o experimental. Portanto, para os experimentos práticos, vamos focar no comportamento do transistor em função de mudanças na temperatura ao invés do erro entre os valores projetados e medidos.

A partir da Figura~\ref{fig:ce-ac-arduino}, medimos que, ao aquecer o transistor, o ganho foi reduzido em \SI{10.2}{\percent} e o ponto de operação caiu cerca de \SI{8}{\percent}. Como esperado, houve uma redução no ganho e no ponto de operação ao aumentar a temperatura do transistor. O erro alto no ganho deve-se à alta sensibilidade ao valor de \B, que muda em função da temperatura. O erro no ponto de operação deve-se à alta sensibilidade da tensão na base, que também é sensível à mudanças na temperatura, visto que ao aquecer o transistor estamos aumentando a concentração de portadores de carga nas junções, que, por sua vez, aumenta a corrente $i_c$ e diminui o valor da tensão no coletor.

\begin{figure}
    \centering
    \begin{subfigure}{0.45\textwidth}
        \includegraphics[width=\textwidth]{images/practice/common-emitter/ce-ac-60.png}
        \caption{$T_a$}
        \label{fig:ce-ac-amb-arduino}
    \end{subfigure}
    \begin{subfigure}{0.45\textwidth}
        \includegraphics[width=\textwidth]{images/practice/common-emitter/ce-ac-60-hot.png}
        \caption{$T_q$}
        \label{fig:ce-ac-hot-arduino}
    \end{subfigure}
    \caption{Medida do sinal de entrada $v_i$ (azul), da tensão na base $v_B$ (verde) e no coletor $v_C$ (vermelho) para o transistor na temperatura ambiente $T_a$ (esquerda) e após aquecê-lo com o ferro de solda $T_q$ (direita). Para o transistor na temperatura ambiente, temos que o ganho total vale $G_v(T_a) = \num{-47.929}$ e o ponto de operação na saída vale $V_C(T_a) = \SI{2.499}{\volt}$. Após aquecer o transistor com o ferro de solda, obtivemos que $G_v(T_q) = \num{-43.041}$ e $V_C(T_q) = \SI{2.299}{\volt}$}
    \label{fig:ce-ac-arduino}
\end{figure}
