\subsection{Polarização com Divisor Resistivo}\label{sec:theory-divider}

Substituindo o resistor \RB do circuito da Figura~\ref{fig:degen} por $\RB = \SI{15}{\kilo\ohm}$ e adicionando um potenciômetro de \SI{10}{\kilo\ohm} e 20 voltas \RBB entre o terminal da base e o terra, obtemos o circuito da Figura~\ref{fig:divider}, que polariza o transistor através de um divisor resistivo. Queremos encontrar o valor para \RBB de forma que tenhamos a máxima excursão de saída $V_C = \SI{2.5}{\volt}$.

\begin{figure}
  \center
  \scalebox{1.0}{
  \begin{circuitikz}
    \draw node[npn, label=right:$Q$] (Q) {};
    \draw (Q.E) to[R, l=$R_E$] ++(0, -2.25) -- ++(-1.8, 0) node[ground] (G) {} -- ++(-2, 0) to[sV, l=$v_i$] ++(0, 3) to[C, l=$C$] ++(2, 0) node[circ, label=45:$V_B$] (Cright) {};
    \draw (Cright) -- ++(0, -0.75) to[R, l=\RBB] (G);
    \draw (Q.B) -- (Cright.center);
    \draw (Cright.center) -- ++(0, 0.75) to[R, l=$\RB$] ++(0, 2) node (RBup) {} -- ++(1.8, 0) node (RCup) {} to[R, l=$R_{C}$] (Q.C);
    \draw (RBup.center) -- ++(-0.5, 0) node[label=left:$V_{CC}$] {};
    \draw (RCup.center) -- ++(0.5, 0);
    \draw (Q.C) -- ++(0.5, 0) node[ocirc, label=right:$V_C$] {};
  \end{circuitikz}
  }
  \caption{Transistor bipolar NPN na configuração emissor-comum com ou um resistor de degeneração de emissor.}
  \label{fig:divider}
\end{figure}

\subsubsection{Análise de Grandes Sinais}\label{sec:ls-theory-divider}

O cálculo da corrente $I_C$ e tensão $V_{BE}$ é o mesmo das Seções~\ref{sec:ls-theory-ce} e~\ref{sec:ls-theory-degen}, de onde $I_C \approx \SI{2.083}{\milli\ampere}$ e $V_{BE} \approx \SI{647}{\milli\volt}$. Análogo à Seção~\ref{sec:ls-theory-degen}, a tensão $V_B$ na base do transistor deve valer $V_B = \SI{1.626}{\volt}$. Se desprezarmos a corrente $I_B$ que passa pela base do transistor, podemos escolher \RBB que satisfaça

\begin{equation}
    V_B = V_{CC} \frac{\RBB}{\RB + \RBB}, 
\end{equation}

de onde temos

\begin{equation}
    \RBB = \RB \frac{V_B}{V_{CC} - V_B} =  \SI{15}{\kilo\ohm} \frac{\SI{1.626}{\volt}}{\SI{5}{\volt} - \SI{1.626}{\volt}} \approx \fbox{\SI{7233}{\ohm}}.
\end{equation}

O esquemático do circuito simulado é dado pela Figura~\ref{fig:sim-divider-dc} e os valores medidos para $V_C$, $V_B$ e $V_E$ são dados pela Figura~\ref{fig:sim-divider-dc-sig}. A partir das medidas, podemos calcular que

\begin{align}
    I_C &= \frac{V_{CC} - V_C}{R_C} = \frac{\SI{5}{\volt} - \SI{2.629}{\volt}}{\SI{1.2}{\kilo\ohm}} \approx \fbox{\SI{1.976}{\milli\ampere}}, \\
    I_B &= \frac{V_{CC} - V_B}{\RB} - \frac{V_B}{\RBB} = \frac{\SI{5}{\volt} - \SI{1.592}{\volt}}{\SI{15}{\kilo\ohm}} - \frac{\SI{1.592}{\volt}}{\SI{7233}{\ohm}} \approx \fbox{\SI{7.098}{\micro\ampere}}, \\
    I_E &= \frac{V_{E}}{R_E} = \frac{\SI{932}{\milli\volt}}{\SI{470}{\ohm}} \approx \fbox{\SI{1.983}{\milli\ampere}}, \\
    \alpha_{CC} &= \frac{I_C}{I_E} = \frac{\SI{1.976}{\milli\ampere}}{\SI{1.983}{\milli\ampere}} \approx \num{0.9965} \\
    \B &= \frac{\alpha_{CC}}{1 - \alpha_{CC}} = \frac{\num{0.9965}}{1 - \num{0.9965}} \approx \fbox{\num{282.3}}.
\end{align}

Note como o erro na tensão de saída $V_C$ caiu de \SI{18}{\percent} para somente \SI{5}{\percent} ao utilizar um divisor resistivo para polarizar o transistor. Isso se deve ao fato que com o divisor resistivo, temos um controle muito maior sobre a tensão $V_{BE}$, visto o valor projetado para $V_E$ é praticamente indiferente a \B e a tensão $V_B$ quase que independe das características intrínsecas do resistor, sendo controlada pelo divisor resistivo entre \RB e \RBB. Dessa forma, conseguimos uma tensão $V_{BE}$ muito mais estável, que depende muito pouco de \B e permite-nos projetar o ponto de operação com mais confiança.

\begin{figure}
    \centering
    \includegraphics[width=0.6\textwidth]{images/theory/divider/divider-circuit-dc.png}
    \caption{Esquemático do transistor na configuração emissor-comum dado pela Figura~\ref{fig:divider} com sinal AC zerado e simulado usando o PSpice. O circuito foi simulado usando uma análise do tipo transiente com TSTOP = \SI{100}{\milli\second}, Max Step Size = \SI{10}{\micro\second} e Start Saving Data After = \SI{10}{\milli\second} na temperatura $T = \SI{25}{\celsius}$.}
    \label{fig:sim-divider-dc}
\end{figure}

\begin{figure}
  \centering
  \includegraphics[width=\textwidth]{images/theory/divider/divider-dc-25C.png}
  \caption{Medida da tensão nos terminais do coletor (verde), base (vermelho) e emissor (azul) na simulação do esquemático da Figura~\ref{fig:sim-divider-dc}. Temos que a tensão no terminal do coletor vale $V_C = \SI{2.629}{\volt}$ (Y1, V(RC:1)), a tensão na base vale $V_B = \SI{1.592}{\milli\volt}$ (Y1, V(RB1:1)) e no emissor $V_E = \SI{932}{\milli\volt}$ (Y1, V(Q:E)).} 
  \label{fig:sim-divider-dc-sig}
\end{figure}

\subsubsection{Análise de Pequenos Sinais}\label{sec:ss-theory-divider}

Análogo às Seções~\ref{sec:ss-theory-ce} e~\ref{sec:ss-theory-degen}, temos que a transcondutância $g_m$ e a resistência $r_\pi$ no modelo $\pi$-híbrido dado pela Figura~\ref{fig:pi-divider} valem  $g_m \approx \fbox{\SI{81}{\milli\ampere\per\volt}}$ e $r_\pi \approx \fbox{\SI{4318}{\ohm}}$, respectivamente.

A impedância de entrada $R_\text{in}$ tem apenas a resistência \RBB acrescentada em paralelo à expressão obtida na Seção~\ref{sec:ss-theory-degen}, de onde temos que

\begin{equation}
    R_\text{in} = \RB \|\; \RBB \|\; \Rinb = \RB \|\; \RBB \|\; [r_\pi + (\B + 1)R_E] \approx \fbox{\SI{4743}{\ohm}}
\end{equation}

A frequência de passagem $f_0$ é dada pela mesma relação usada anteriormente, de onde temos que

\begin{equation}
    f_0 = \frac{1}{2 \pi \cdot \SI{1}{\micro\farad} \cdot \SI{4743}{\kilo\ohm}} \approx \fbox{\SI{36}{\hertz}}.
\end{equation}

O cálculo do ganho $A_v$ do transistor independe do acréscimo do resistor \RBB, de forma que podemos usar o mesmo valor computado na Seção~\ref{sec:ss-theory-degen}, tendo $A_v \approx \fbox{\num{-2.481}}$. 

Assim, temos que o ganho total do amplificador vale

\begin{equation}
    {G_v = A_v \frac{R_\text{in}}{R_\text{ger} + R_\text{in}} \approx \fbox{\num{-2.455}}}.
\end{equation}

\begin{figure}
  \center
  \scalebox{1.0}{
  \begin{circuitikz}[american, american currents]
    \draw (-2.0, 0) node[ocirc, label=$B$] (B) {} -- ++(1.5, 0) node (RBup) {} -- ++(1.5, 0) to[R, l=$r_\pi$, v=$v_\pi$] ++(0, -2) -- ++(1, 0) node[ocirc, label=$E$] (E) {} -- ++(1, 0) node (Eright) {};
    \draw (B) to[R, l_=\RB] ++(0, -2) node[ground] {};
    \draw (RBup.center) to[R, l_=\RBB] ++(0, -2) node[ground] {};
    \draw (E.south) to[R, l=$R_E$] ++(0, -2) node[ground] {};
    \draw (6, 0) node[ocirc, label=$C$] {} -- ++(-1, 0) node (RC) {} -- ++(-2, 0) to[cisource, l=$g_m v_\pi$] (Eright.center);
    \draw (RC.center) to[R, l=$R_C$] ++(0, -2) node[ground] {};
    \draw (B) to[R, l=$R_\text{ger}$] ++(-3, 0) to[sV, l_=$v_i$] ++(0, -2) node[ground] {};
  \end{circuitikz}
  }
  \caption{Modelo $\pi$-híbrido de pequenos sinais para o transistor na configuração emissor-comum com resistor de degeneração de emissor dado pela Figura~\ref{fig:degen}.}
  \label{fig:pi-divider}
\end{figure}


O esquemático do circuito com um sinal AC de entrada simulado no PSpice para as temperaturas \temps é dado pela Figura~\ref{fig:sim-divider-ac}. As medidas do sinal de entrada $v_i$ e de saída $v_c$ para as diferentes temperaturas são dadas pela Figura~\ref{fig:sim-divider-ac-sig}. A partir das medidas, temos que $G_v(\SI{25}{\celsius}) = \num{-2.193}$, um valor \SI{12}{\percent} menor do que o projetado $G_v = \num{-2.455}$. Dessa vez, houve um aumento no erro do ganho projetado em relação ao circuito com apenas o resistor de degeneração de emissor, mas o erro ainda é cerca de 3 vezes menor que o do circuito inicial. Além disso, com o divisor resistivo a impedância de entrada $R_\text{in}$ diminui em cerca de 30 vezes, que não é um efeito desejado para circuito.

Pela Figura~\ref{fig:sim-divider-ac-sig}, também podemos ver o efeito da temperatura na tensão $V_C$ para o ponto de operação. Um decréscimo de \SI{5}{\percent} na temperatura provocou uma aumento de \SI{2.45}{\percent} na tensão $V_C$, enquanto um aumento de \SI{5}{\percent} provocou um decréscimo de \SI{2.45}{\percent}. Note como mudança a mudança no novo circuito há uma sensibilidade maior do ponto de operação em função da temperatura, apesar do valor ser menos sensível às características intrínsecas do transistor. O ganho aumentou \SI{0.09}{\percent} para um decréscimo de \SI{5}{\percent} na temperatura e diminuiu \SI{0.04}{\percent} para um acréscimo de \SI{5}{\percent}. Dessa forma, temos que o divisor resistivo ajudou a estabilizar ainda mais o ganho a mudanças na temperatura e no valor de \B, apesar de ter do ganho ser um pouco menos previsível.

% TODO: discutir comportamento AC em simulação

\begin{figure}
    \centering
    \includegraphics[width=0.6\textwidth]{images/theory/divider/divider-circuit-ac.png}
    \caption{Esquemático do transistor na configuração emissor-comum dado pela Figura~\ref{fig:divider} com sinal AC de \SI{10}{\milli\volt} de pico-a-pico centrado em \SI{0}{\volt} e frequência de \SI{100}{\hertz}. O circuito foi simulado usando uma análise do tipo transiente com TSTOP = \SI{100}{\milli\second}, Max Step Size = \SI{10}{\micro\second} e Start Saving Data After = \SI{10}{\milli\second} para as temperaturas \temps.}
    \label{fig:sim-divider-ac}
\end{figure}

\begin{figure}
  \centering
  \includegraphics[width=\textwidth]{images/theory/divider/divider-ac-avg-10C-25C-40C.png}
  \caption{Formas de onda do sinal de entrada $v_i$ (ciano) e os sinais de saída $v_c$ para as temperaturas $T_f = \SI{10}{\celsius}$ (verde), $T_a = \SI{25}{\celsius}$ (azul) e $T_q = \SI{40}{\celsius}$ (amarelo) na simulação do esquemático da Figura~\ref{fig:sim-divider-ac}. Para cada temperatura $T$, mediu-se o ganho de tensão total do amplificador $G_v(T)$ a partir da razão $v_{c_{pp}} / v_{i_{pp}}$, obtendo-se que $G_v(T_f) = \num{-2.195}$ (Y1 - Y2, V(RC: 1)), $G_v(T_a) = \num{-2.193}$ (Y1 - Y2, V(RC: 1)) e $G_v(T_q) = \num{-2.192}$ (Y1 - Y2, V(RC: 1)). A tensão de saída para cada temperatura vale $V_C(T_f) = \SI{2.694}{\volt}$ (Avg Y, V(RC: 1)) $V_C(T_a) = \SI{2.629}{\volt}$ (Avg Y, V(RC: 1)) e $V_C(T_q) = \SI{2.565}{\volt}$ (Avg Y, V(RC: 1)).}
  \label{fig:sim-divider-ac-sig}
\end{figure}