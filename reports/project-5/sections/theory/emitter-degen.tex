\subsection{Resistor de Degeneração de Emissor}\label{sec:theory-degen}

\newcommand{\Rinb}{\ensuremath{R_{\text{in}_b}}}

Adicionando um resistor $R_E$ entre o terminal do emissor e o terra, obtemos o circuito da Figura~\ref{fig:degen}. Usando o valor sugerido do enunciado para $R_E$ que vale \SI{470}{\ohm}, queremos encontrar o novo valor de \RB de forma que tenhamos a máxima excursão de saída (\ie $V_C = \SI{2.5}{\volt}$).

\begin{figure}
  \center
  \scalebox{1.0}{
  \begin{circuitikz}
    \draw node[npn, label=right:$Q$] (Q) {};
    \draw (Q.E) to[R, l=$R_E$] ++(0, -2.25) -- ++(-1.8, 0) node[ground] {} -- ++(-2, 0) to[sV, l=$v_i$] ++(0, 3) to[C, l=$C$] ++(2, 0) node[circ, label=45:$V_B$] (Cright) {};
    \draw (Q.B) -- (Cright.center);
    \draw (Cright.center) -- ++(0, 0.75) to[R, l=$\RB$] ++(0, 2) node (RBup) {} -- ++(1.8, 0) node (RCup) {} to[R, l=$R_{C}$] (Q.C);
    \draw (RBup.center) -- ++(-0.5, 0) node[label=left:$V_{CC}$] {};
    \draw (RCup.center) -- ++(0.5, 0);
    \draw (Q.C) -- ++(0.5, 0) node[ocirc, label=right:$V_C$] {};
  \end{circuitikz}
  }
  \caption{Transistor bipolar NPN na configuração emissor-comum com ou um resistor de degeneração de emissor.}
  \label{fig:degen}
\end{figure}

\subsubsection{Análise de Grandes Sinais}\label{sec:ls-theory-degen}

O cálculo da corrente $I_C$ e da tensão $V_{BE}$ é análogo ao apresentado na Seção~\ref{sec:ls-theory-ce} e temos que $I_C \approx \SI{2.083}{\milli\ampere}$ e $V_{BE} \approx \SI{647}{\milli\volt}$. Com a introdução de $R_E$, a tensão $V_E$ não vale mais \SI{0}{\volt} e é dada por $V_E = I_E R_E$, onde a corrente $I_E = I_C \frac{\B + 1}{\B} \approx I_C$. Dessa forma, $V_E \approx \SI{979}{\milli\volt}$ e $V_B = V_E + V_{BE} \approx \SI{1.626}{\volt}$. Pela lei de Ohm, temos que $\RB = \frac{V_{CC} - V_B}{I_B} = \B \frac{V_{CC} - V_B}{I_C} \approx \SI{567}{\kilo\ohm}$.


O esquemático do circuito simulado é dado pela Figura~\ref{fig:sim-degen-dc} e os valores medidos para $V_C$, $V_B$ e $V_E$ são dados pela Figura~\ref{fig:sim-degen-dc-sig}. A partir das medidas, podemos calcular que

\begin{align}
    I_C &= \frac{V_{CC} - V_C}{R_C} = \frac{\SI{5}{\volt} - \SI{2.893}{\volt}}{\SI{1.2}{\kilo\ohm}} \approx \fbox{\SI{1.756}{\milli\ampere}}, \\
    I_B &= \frac{V_{CC} - V_B}{\RB} = \frac{\SI{5}{\volt} - \SI{1.485}{\volt}}{\SI{567}{\kilo\ohm}} \approx \fbox{\SI{6.199}{\micro\ampere}}, \\
    I_E &= \frac{V_{E}}{R_E} = \frac{\SI{828}{\milli\volt}}{\SI{470}{\ohm}} \approx \fbox{\SI{1.762}{\milli\ampere}}, \\
    \alpha_{CC} &= \frac{I_C}{I_E}  = \frac{\SI{1.756}{\milli\ampere}}{\SI{1.762}{\milli\ampere}} \approx \num{0.9966} \\
    \B &= \frac{\alpha_{CC}}{1 - \alpha_{CC}} = \frac{\num{0.9966}}{1 - \num{0.9966}} \approx \fbox{\num{292.7}}.
\end{align}

Novamente, há uma grande discrepância entre o valor de \B medido com o multímetro e o da simulação. Vale notar que também há uma diferença entre os valores de \B medidos nos circuitos das Figuras~\ref{fig:sim-ce-dc} e~\ref{fig:sim-degen-dc}. Isso se deve à dependência de \B com a corrente $I_C$, que aumentou cerca de \SI{2.8}{\percent}. com a introdução do resistor de degeneração, aproximando-se do valor teórico calculado com o \B medido pelo multímetro. Apesar de haver uma pequena melhoria, ela se deve principalmente às aproximações usadas no projeto do resistor \RB e não ajuda a minimizar o efeito da diferença no valor de \B significativamente. Ainda temos linear na tensão $V_C$, que é cerca de \SI{18}{\percent} maior para um \B que é \SI{18}{\percent} menor que o usado no projeto.

% TODO: Explicar o motivo das discrepâncias

\begin{figure}
    \centering
    \includegraphics[width=0.6\textwidth]{images/theory/emitter-degen/emitter-degen-circuit-dc.png}
    \caption{Esquemático do transistor na configuração emissor-comum dado pela Figura~\ref{fig:degen} com sinal AC zerado e simulado usando o PSpice. O circuito foi simulado usando uma análise do tipo transiente com TSTOP = \SI{100}{\milli\second}, Max Step Size = \SI{10}{\micro\second} e Start Saving Data After = \SI{10}{\milli\second} na temperatura $T = \SI{25}{\celsius}$.}
    \label{fig:sim-degen-dc}
\end{figure}

\begin{figure}
  \centering
  \includegraphics[width=\textwidth]{images/theory/emitter-degen/emmitter-degen-dc-25C.png}
  \caption{Medida da tensão nos terminais do coletor (verde), base (vermelho) e emissor (azul) na simulação do esquemático da Figura~\ref{fig:sim-degen-dc}. Temos que a tensão no terminal do coletor vale $V_C = \SI{2.893}{\volt}$ (Y1, V(RC:1)), a tensão na base vale $V_B = \SI{1.485}{\milli\volt}$ (Y1, V(RB1:1)) e no emissor $V_E = \SI{828}{\milli\volt}$ (Y1, V(Q:E)).} 
  \label{fig:sim-degen-dc-sig}
\end{figure}

\subsubsection{Análise de Pequenos Sinais}\label{sec:ss-theory-degen} 

Usando o modelo $\pi$-híbrido da Figura~\ref{fig:pi-degen} e a Equação~\ref{eq:gm}, vemos que o cálculo de $g_m$ é igual ao da Seção~\ref{sec:ss-theory-ce} e vale $g_m \approx \fbox{\SI{81}{\milli\ampere\per\volt}}$. A resistência $r_\pi$ também permanece inalterada e vale $r_\pi \approx \fbox{\SI{4318}{\ohm}}$.

A impedância de entrada $R_\text{in}$ representa o efeito de carga do amplificador no sinal de entrada $v_i$. Pelo modelo da Figura~\ref{fig:pi-degen}, temos que $R_\text{in} = \frac{v_x}{i_x}$. Por inspeção, temos que $R_\text{in} = \RB \|\; \Rinb$, onde \Rinb representa o efeito da carga do amplificador no sinal vindo da base. Portanto, resta-nos calcular o valor \Rinb, que é dado por

\begin{align*}
    \Rinb = \frac{v_x}{i_b} &= \frac{v_\pi + v_e}{v_\pi / r_\pi} = \frac{v_\pi + v_\pi R_E (g_m + 1 / r_\pi)}{v_\pi / r_\pi} \\
    &= r_\pi (1 + R_E(g_m + 1 / r_\pi)) \\
    &= r_\pi \left(1 + R_E \left(\frac{\B + 1}{r_\pi}\right)\right) \\
    &= r_\pi + (\B + 1) R_E.
\end{align*}

Assim, temos que $R_\text{in}$ vale

\begin{equation}\label{eq:rin-degen}
    R_\text{in} = \RB \|\; \Rinb = \RB \|\; [r_\pi + (\B + 1) R_E] \approx \fbox{\SI{130}{\kilo\ohm}}.
\end{equation}

A frequência de passagem $f_0$ para o circuito passa-altas da Figura~\ref{fig:degen} é dada pela mesma relação apresentada na Seção~\ref{sec:ss-theory-ce}, de onde temos que

\begin{equation}
    f_0 = \frac{1}{2 \pi \cdot \SI{1}{\micro\farad} \cdot \SI{130}{\kilo\ohm}} \approx \fbox{\SI{1.2}{\hertz}}.
\end{equation}

Seguindo os mesmos passos da Seção~\ref{sec:ss-theory-ce}, vamos primeiro calcular o ganho $A_v$ do transistor e depois o ganho total $G_v$ do amplificador. Assim, temos que

\begin{align}
    A_v = \frac{v_c}{v_b} &= \frac{-g_m v_\pi R_C}{v_\pi + v_e} \\
    &= \frac{-g_m v_\pi R_C}{v_\pi (1 + R_E(g_m + 1 / r_\pi)} \\
    &= \frac{-g_m R_C}{1 + g_m R_E \frac{\B + 1}{\B}} \label{eq:degen-gain} \\
    &\approx \fbox{\num{-2.481}}
\end{align}

Note como a nova expressão para o ganho é pouco sensível ao ganho de corrente DC do transistor \B~e à transcondutância $g_m$.

O ganho total do amplificador é 

\begin{equation}
    {G_v = A_v \frac{R_\text{in}}{R_\text{ger} + R_\text{in}} \approx \fbox{\num{-2.480}}}.
\end{equation}

\begin{figure}
  \center
  \scalebox{1.0}{
  \begin{circuitikz}[american, american currents]
    \draw (-0.5, 0) node[ocirc, label=$B$] (B) {} to[short, i=$i_b$] ++(2, 0) to[R, l=$r_\pi$, v=$v_\pi$] ++(0, -2) -- ++(1, 0) node[ocirc, label=$E$] (E) {} -- ++(1, 0) node (Eright) {};
    \draw (B) to[R, l=\RB, v=$v_x$] ++(0, -2) node[ground] {};
    \draw (E.south) to[R, l=$R_E$, v=$v_e$] ++(0, -2) node[ground] {};
    \draw (6.5, 0) node[ocirc, label=$C$] {} -- ++(-1, 0) node (RC) {} -- ++(-2, 0) to[cisource, l=$g_m v_\pi$] (Eright.center);
    \draw (RC.center) to[R, l=$R_C$] ++(0, -2) node[ground] {};
    \draw (B) to[R, l=$R_\text{ger}$, i^<=$i_x$] ++(-3, 0) to[sV, l_=$v_i$] ++(0, -2) node[ground] {};
  \end{circuitikz}
  }
  \caption{Modelo $\pi$-híbrido de pequenos sinais para o transistor na configuração emissor-comum com resistor de degeneração de emissor dado pela Figura~\ref{fig:degen}.}
  \label{fig:pi-degen}
\end{figure}

O esquemático do circuito com um sinal AC de entrada simulado no PSpice para as temperaturas \temps é dado pela Figura~\ref{fig:sim-degen-ac}. As medidas do sinal de entrada $v_i$ e de saída $v_c$ para as diferentes temperaturas são dadas pela Figura~\ref{fig:sim-degen-ac-sig}. A partir das medidas, temos que $G_v(\SI{25}{\celsius}) = \num{-2.457}$, um valor apenas \SI{0.9}{\percent} menor do que o projetado $G_v = \num{-2.480}$. Note como houve uma redução drástica no erro e o ganho tornou-se muito mais previsível com a introdução do resistor de degeneração de emissor. Isso ocorreu porque a nova expressão do ganho dada pela Equação~\ref{eq:degen-gain} quase não depende de \B, que aparece no denominador em um termo que sempre será muito próximo de 1, $\frac{\B + 1}{\B}$. Além disso, o novo ganho não depende tanto da transcondutância, que aparece tanto no numerador como no denominador da expressão, sendo menos sensível às mudanças em temperatura e no ponto de operação do transistor. O ganho é majoritariamente controlado pelo valor das resistências $R_C$ e $R_E$.

Pela Figura~\ref{fig:sim-degen-ac-sig}, também podemos ver o efeito da temperatura na tensão $V_C$ para o ponto de operação. Um decréscimo de \SI{5}{\percent} na temperatura provocou uma aumento de \SI{0.52}{\percent} na tensão $V_C$, enquanto um aumento de \SI{5}{\percent} provocou um decréscimo de \SI{0.52}{\percent}. Note como mudança no ponto de operação é igual à do circuito da Figura~\ref{fig:sim-ce-ac}, mas dessa vez a mudança no ganho é muito menor em função da alteração no ponto de operação e na temperatura. O ganho aumentou \SI{0.12}{\percent} para um decréscimo de \SI{5}{\percent} na temperatura e diminuiu \SI{0.16}{\percent} para um acréscimo de \SI{5}{\percent} na temperatura. Dessa forma, temos que o resistor de degeneração de emissor ajudou a estabilizar o ganho a mudanças tanto no valor de \B como na temperatura, apesar de ainda estar sujeito a erros no ponto de operação.

% TODO: discutir como o beta não afeta tanto o ganho e a temperatura também não. Porém, agora a temperatura afeta o ponto de operação e antes n afetava. Pq?

\begin{figure}
    \centering
    \includegraphics[width=0.6\textwidth]{images/theory/emitter-degen/emitter-degen-circuit-ac.png}
    \caption{Esquemático do transistor na configuração emissor-comum dado pela Figura~\ref{fig:degen} com sinal AC de \SI{10}{\milli\volt} de pico-a-pico centrado em \SI{0}{\volt} e frequência de \SI{100}{\hertz}. O circuito foi simulado usando uma análise do tipo transiente com TSTOP = \SI{100}{\milli\second}, Max Step Size = \SI{10}{\micro\second} e Start Saving Data After = \SI{10}{\milli\second} para as temperaturas \temps.}
    \label{fig:sim-degen-ac}
\end{figure}

\begin{figure}
  \centering
  \includegraphics[width=\textwidth]{images/theory/emitter-degen/emitter-degen-ac-avg-10C-25C-40C.png}
  \caption{Formas de onda do sinal de entrada $v_i$ (ciano) e os sinais de saída $v_c$ para as temperaturas $T_f = \SI{10}{\celsius}$ (verde), $T_a = \SI{25}{\celsius}$ (azul) e $T_q = \SI{40}{\celsius}$ (amarelo) na simulação do esquemático da Figura~\ref{fig:sim-degen-ac}. Para cada temperatura $T$, mediu-se o ganho de tensão total do amplificador $G_v(T)$ a partir da razão $v_{c_{pp}} / v_{i_{pp}}$, obtendo-se que $G_v(T_f) = \num{-2.460}$ (Y1 - Y2, V(RC: 1)), $G_v(T_a) = \num{-2.457}$ (Y1 - Y2, V(RC: 1)) e $G_v(T_q) = \num{-2.453}$ (Y1 - Y2, V(RC: 1)). A tensão de saída para cada temperatura vale $V_C(T_f) = \SI{2.908}{\volt}$ (Avg Y, V(RC: 1)) $V_C(T_a) = \SI{2.893}{\volt}$ (Avg Y, V(RC: 1)) e $V_C(T_q) = \SI{2.878}{\volt}$ (Avg Y, V(RC: 1)).}
  \label{fig:sim-degen-ac-sig}
\end{figure}