\subsection{Emissor Comum}\label{sec:theory-ce}

\newcommand{\RB}{\ensuremath{R_{B_1}} }
\newcommand{\RBB}{\ensuremath{R_{B_2}} }
\newcommand{\B}{\ensuremath{\beta_{CC}} }
\newcommand{\temps}{\ensuremath{T \in \{\SI{10}{\celsius}, \SI{25}{\celsius}, \SI{40}{\celsius}\}} }

O circuito com o transistor bipolar na configuração emissor-comum é dado pela Figura~\ref{fig:ce}. Queremos calcular o valor do resistor \RB para se obter a máxima excursão do sinal de saída, \ie $V_C = V_{CC} / 2$. Alimentando o circuito com a fonte do arduino~\cite{arduino} que vale $V_{CC} = \SI{5}{\volt}$, devemos projetar o circuito de forma que $V_C = \SI{2.5}{\volt}$.

\begin{figure}
  \center
  \scalebox{1.0}{
  \begin{circuitikz}
    \draw node[npn, label=right:$Q$] (Q) {};
    \draw (Q.E) -- ++(0, -2.25) -- ++(-1.8, 0) node[ground] {} -- ++(-2, 0) to[sV, l=$v_i$] ++(0, 3) to[C, l=$C$] ++(2, 0) node[circ, label=45:$V_B$] (Cright) {};
    \draw (Q.B) -- (Cright.center);
    \draw (Cright.center) -- ++(0, 0.75) to[R, l=$\RB$] ++(0, 2) node (RBup) {} -- ++(1.8, 0) node (RCup) {} to[R, l=$R_{C}$] (Q.C);
    \draw (RBup.center) -- ++(-0.5, 0) node[label=left:$V_{CC}$] {};
    \draw (RCup.center) -- ++(0.5, 0);
    \draw (Q.C) -- ++(0.5, 0) node[ocirc, label=right:$V_C$] {};
  \end{circuitikz}
  }
  \caption{Transistor bipolar NPN na configuração emissor-comum.}
  \label{fig:ce}
\end{figure}

\subsubsection{Análise de Grandes Sinais}\label{sec:ls-theory-ce}

Usando o valor de $R_C = \SI{1.2}{\kilo\ohm}$ sugerido no enunciado, podemos calcular a corrente no coletor $I_C$ pela lei de Ohm, de onde temos que $I_C = \frac{V_{CC} - V_C}{R_C} \approx \SI{2.083}{\milli\ampere}$. Pelo datasheet do transistor BC548B~\cite{bjt-datasheet}, temos que o ganho de corrente DC vale tipicamente $\B = 290$. Porém, para obtermos uma estimativa mais próxima do circuito real, medimos o valor de $\B$ usando o multímetro, com o qual obtivemos que $\B = 350$. Usando o valor medido para \B, temos que a corrente na base do transistor $I_B$ deve valer $I_B = \frac{I_C}{\B} \approx \SI{5.952}{\micro\ampere}$. A tensão $V_{BE}$ entre a base e o emissor é dada por

\begin{equation}\label{eq:vbe}
    V_{BE} = V_T \ln \frac{I_C}{I_S},
\end{equation}

onde $V_T = \frac{kT}{q}$ é a tensão térmica que vale aproximadamente \SI{25.7}{\milli\volt} na temperatura ambiente ($T = \SI{298}{\kelvin} = \SI{25}{\celsius}$) e $I_S$ é a corrente de saturação do transistor. Pelo modelo SPICE do transistor BC548B obtido do PSpice da OrCAD~\cite{pspice}, obtivemos que a corrente de saturação $I_S = \SI{2.39e-14}{\ampere}$, com o qual calculamos que $V_{BE} \approx \SI{647}{\milli\volt}$. Assim, podemos calcular \RB como $\frac{V_{CC} - V_{B}}{I_B} \approx \SI{731}{\kilo\ohm}$.

O esquemático do circuito simulado no PSpice~\cite{pspice} é dado pela Figura~\ref{fig:sim-ce-dc} e os valores medidos para $V_C$ e $V_B$ são dados pela Figura~\ref{fig:sim-ce-dc-sig}. A partir das medidas, podemos calcular que

\begin{align}
    I_C &= \frac{V_{CC} - V_C}{R_C} = \frac{\SI{5}{\volt} - \SI{2.951}{\volt}}{\SI{1.2}{\kilo\ohm}} \approx \fbox{\SI{1.708}{\milli\ampere}}, \\
    I_B &= \frac{V_{CC} - V_B}{\RB} = \frac{\SI{5}{\volt} - \SI{656}{\milli\volt}}{\SI{731}{\kilo\ohm}} \approx \fbox{\SI{5.943}{\micro\ampere}}, \\
    \B &= \frac{I_C}{I_B} = \frac{\SI{1.708}{\milli\ampere}}{\SI{5.943}{\micro\ampere}} \approx \fbox{\num{287.4}}.
\end{align}

Como podemos ver, há uma grande discrepância entre o valor de \B medido usando o multímetro e usado para projetar o circuito ($\B = \num{350}$), e o valor de \B medido na simulação ($\B = \num{287.4}$), que é bem próximo do valor nominal do datasheet do transistor~\cite{bjt-datasheet}. Essa discrepância afeta significativamente o valor de $I_C$ no modelo SPICE, que depende linearmente do valor de \B. Um erro de \SI{18}{\percent} no valor de \B usado no projeto acarretou em uma corrente $I_C$ \SI{18}{\percent} menor que a projetada, que, por sua vez, levou a uma tensão $V_C$ \SI{18}{\percent} maior que a projetada. Dessa forma, não fomos bem sucedidos em obter a máxima excursão de saída.

\begin{figure}
    \centering
    \includegraphics[width=0.6\textwidth]{images/theory/common-emitter/common-emitter-circuit-dc.png}
    \caption{Esquemático do transistor na configuração emissor-comum dado pela Figura~\ref{fig:ce} com sinal AC zerado e simulado usando o PSpice. O circuito foi simulado usando uma análise do tipo transiente com TSTOP = \SI{100}{\milli\second}, Max Step Size = \SI{10}{\micro\second} e Start Saving Data After = \SI{10}{\milli\second} na temperatura $T = \SI{25}{\celsius}$.}
    \label{fig:sim-ce-dc}
\end{figure}

\begin{figure}
  \centering
  \includegraphics[width=\textwidth]{images/theory/common-emitter/common-emmitter-dc-25C.png}
  \caption{Medida da tensão nos terminais do coletor (verde) e da base (vermelho) na simulação do esquemático da Figura~\ref{fig:sim-ce-dc}. Em verde pontilhado é apresentada a tensão no terminal do coletor, que vale $V_C = \SI{2.951}{\volt}$ (Y1, V(RC:1)), e em vermelho a tensão na base, que vale $V_B = \SI{656}{\milli\volt}$ (Y1, V(RB1:1)).} 
  \label{fig:sim-ce-dc-sig}
\end{figure}
            
\subsubsection{Análise de Pequenos Sinais}\label{sec:ss-theory-ce}

Para realizar a análise de pequenos sinais do circuito da Figura~\ref{fig:ce}, vamos empregar o modelo $\pi$-híbrido dado pela Figura~\ref{fig:pi-ce}, onde aterramos as fontes DC e os terminais do transistor estão representados por $B$, $C$ e $E$ para a base, coletor e emissor, respectivamente. É importante notar que o modelo de pequenos sinais apresentado só é válido quando $v_{be} \ll V_T$, de forma que seja possível desconsiderar os termos de maior ordem da aproximação por série da relação exponencial entre a corrente $i_c$ que passa pelo coletor e a tensão $v_{be}$ entre a base e o emissor. Para usar o modelo $\pi$-híbrido, devemos calcular os valores de $r_\pi$ e $g_m$ para o ponto de operação do transistor. Nesse modelo, a transcondutância $g_m$ relaciona a corrente pelo coletor $i_c$ com a tensão entre a base e o emissor $v_{be}$ e é dada por

\begin{equation}\label{eq:gm}
    g_m = \frac{I_C}{V_T},
\end{equation}
 
onde $I_C$ é a corrente que passa pelo coletor no ponto de operação do transistor. A resistência $r_\pi$ entre a base e o emissor relaciona a corrente $i_b$ que passa pela base com a tensão $v_{be}$. Visto que $i_c = i_b \; \B$, podemos expressar $r_\pi$ em função da transcondutância como

\begin{equation}\label{eq:rpi}
    r_\pi = \frac{\B}{g_m}.
\end{equation}

Dado o valor de $I_C$ calculado na análise de grandes sinais da Seção~\ref{sec:ls-theory-ce} e assumindo uma temperatura de \SI{25}{\celsius}, temos que a transcondutância vale

\begin{equation}
    g_m = \frac{\SI{2.083}{\milli\ampere}}{\SI{25.7}{\milli\volt}} \approx \fbox{\SI{81}{\milli\ampere\per\volt}},
\end{equation}

de onde também temos que

\begin{equation}
    r_\pi = \frac{350}{\SI{81}{\milli\ampere\per\volt}} \approx \fbox{\SI{4318}{\ohm}}.
\end{equation}

Para obter a impedância de entrada $R_\text{in}$ do circuito vista da base do transistor, podemos usar o modelo $\pi$-híbrido da Figura~\ref{fig:pi-ce}, de onde temos que

\begin{equation}
    R_\text{in} = \RB \|\; r_\pi \approx \fbox{\SI{4293}{\ohm}}. 
\end{equation}

Como o circuito tem apenas uma componente reativa (um capacitor), ele tem apenas uma constante de tempo $\tau = CR_\text{in}$, que podemos usar para calcular a frequência de passagem (\ie frequência de corte). Já que a tensão de saída é medida sob a resistência $R_\text{in}$, nosso circuito se comporta como um filtro passa-altas para a entrada $v_i$, cuja frequência de passagem vale $f_0 = \nicefrac{1}{2\pi\tau}$. Usando o valor de $C = \SI{1}{\micro\farad}$ sugerido pelo enunciado, temos que

\begin{equation}
    f_0 = \frac{1}{2 \pi \cdot \SI{1}{\micro\farad} \cdot \SI{4293}{\ohm}} \approx \fbox{\SI{37}{\hertz}}.
\end{equation}

Para calcular o ganho total do amplificador $G_v = \frac{v_c}{v_i}$, devemos primeiro calcular o ganho $A_v = \frac{v_c}{v_{b}}$ do transistor.
Pelo modelo de pequenos sinais da Figura~\ref{fig:pi-ce}, temos que $v_c = -g_m v_\pi R_C$ e $v_{b} = v_\pi$, de onde

\begin{equation}
    A_v = -g_m R_C = \fbox{\num{-97.3}}.    
\end{equation}

Para obter o ganho total do amplificador, devemos calcular o valor de $v_\pi$ no modelo de pequenos sinais, que vale $v_\pi = v_i \frac{\RB \|\; r_\pi}{R_\text{ger} + (\RB \|\; r_\pi)}$. Assumindo que a resistência interna do gerador vale $R_\text{ger} = \SI{50}{\ohm}$, temos que

\begin{equation}
    {G_v = A_v \frac{R_\text{in}}{R_\text{ger} + R_\text{in}} \approx \fbox{\num{-96.1}}}.
\end{equation}

\begin{figure}
  \center
  \scalebox{1.0}{
  \begin{circuitikz}[american, american currents]
    \draw (-0.5, 0) node[ocirc, label=$B$] (B) {} -- ++(1.5, 0) to[R, l=$r_\pi$, v=$v_\pi$] ++(0, -2) -- ++(1, 0) node[ocirc, label=$E$] (E) {} -- ++(1, 0) node (Eright) {};
    \draw (B) to[R, l_=\RB] ++(0, -2) node[ground] {};
    \draw (E.south) node[ground] {};
    \draw (6, 0) node[ocirc, label=$C$] {} -- ++(-1, 0) node (RC) {} -- ++(-2, 0) to[cisource, l=$g_m v_\pi$] (Eright.center);
    \draw (RC.center) to[R, l=$R_C$] ++(0, -2) node[ground] {};
    \draw (B) to[R, l=$R_\text{ger}$] ++(-3, 0) to[sV, l_=$v_i$] ++(0, -2) node[ground] {};
  \end{circuitikz}
  }
  \caption{Modelo $\pi$-híbrido de pequenos sinais para o transistor na configuração emissor-comum dado pela Figura~\ref{fig:ce}.}
  \label{fig:pi-ce}
\end{figure}

O esquemático do circuito com um sinal AC de entrada simulado no PSpice para as temperaturas \temps é dado pela Figura~\ref{fig:sim-ce-ac}. As medidas do sinal de entrada $v_i$ e de saída $v_c$ para as diferentes temperaturas são dadas pela Figura~\ref{fig:sim-ce-ac-sig}. A partir das medidas, temos que $G_v(\SI{25}{\celsius}) = \num{-64.791}$, um valor \SI{32.6}{\percent} menor do ganho projetado $G_v = \num{-96.1}$. Essa alta discrepância se deve parcialmente a dependência linear entre o ganho do transistor $A_v$ e a corrente $I_C$, que, por sua vez, depende linearmente de \B. Esperava-se que o ganho medido fosse de cerca de \SI{18}{\percent} menor que o projetado, visto que o $I_C$ é \SI{18}{\percent} menor. O erro de \SI{32.6}{\percent} se deve provavelmente ao modelo de pequenos sinais adotado, que não prevê efeitos indesejados, como o Efeito Early~\cite{sedra}, que contribui para diminuir o ganho do amplificador e poderia ser modelado a partir de um resistor em série com $R_C$ na saída.

A partir da Figura~\ref{fig:sim-ce-ac-sig} também podemos ver a dependência do ganho com a temperatura. Visto que a transcondutância $g_m$ é inversamente proporcional à temperatura $T$ em Kelvin através da tensão térmica $V_T$, esperávamos que quanto maior a temperatura menor seria o ganho. Pelas medidas feitas em simulação, obtivemos um aumento de \SI{3.3}{\percent} no ganho para um decréscimo de \SI{5}{\percent} na temperatura e um decréscimo de \SI{3.1}{\percent} no ganho para um aumento de \SI{5}{\percent} na temperatura. Note como o ganho não varia linearmente com a temperatura, já que a temperatura também afeta o ponto de operação $I_C$ do circuito, visto que ela altera a concentração de portadores nas junções do transistor. Podemos ver o efeito da temperatura no ponto de operação pelas medidas da Figura~\ref{fig:sim-ce-ac-sig}, onde um decréscimo de \SI{5}{\percent} na temperatura provocou uma aumento de \SI{0.48}{\percent} na tensão $V_C$ no ponto de operação, enquanto um aumento de \SI{5}{\percent} na temperatura provocou um decréscimo de \SI{0.51}{\percent}. Isso era de se esperar, visto que quanto menor a temperatura, menor a corrente $I_C$ no coletor, que aumenta a tensão de saída $V_C$ do circuito.

\begin{figure}
    \centering
    \includegraphics[width=0.6\textwidth]{images/theory/common-emitter/common-emitter-circuit-ac.png}
    \caption{Esquemático do transistor na configuração emissor-comum dado pela Figura~\ref{fig:ce} com sinal AC de \SI{10}{\milli\volt} de pico-a-pico centrado em \SI{0}{\volt} e frequência de \SI{100}{\hertz}. O circuito foi simulado usando uma análise do tipo transiente com TSTOP = \SI{100}{\milli\second}, Max Step Size = \SI{10}{\micro\second} e Start Saving Data After = \SI{10}{\milli\second} para as temperaturas \temps.}
    \label{fig:sim-ce-ac}
\end{figure}

\begin{figure}
  \centering
  \includegraphics[width=\textwidth]{images/theory/common-emitter/common-emitter-ac-avg-10C-25C-40C.png}
  \caption{Formas de onda do sinal de entrada $v_i$ (ciano) e os sinais de saída $v_c$ para as temperaturas $T_f = \SI{10}{\celsius}$ (verde), $T_a = \SI{25}{\celsius}$ (azul) e $T_q = \SI{40}{\celsius}$ (amarelo) na simulação do esquemático da Figura~\ref{fig:sim-ce-ac}. Para cada temperatura $T$, mediu-se o ganho de tensão total do amplificador $G_v(T)$ a partir da razão $v_{c_{pp}} / v_{i_{pp}}$, obtendo-se que $G_v(T_f) = \num{-67.000}$ (Y1 - Y2, V(RC: 1)), $G_v(T_a) = \num{-64.791}$ (Y1 - Y2, V(RC: 1)) e $G_v(T_q) = \num{-62.751}$ (Y1 - Y2, V(RC: 1)). A tensão de saída para cada temperatura vale $V_C(T_f) = \SI{2.958}{\volt}$ (Avg Y, V(RC: 1)) $V_C(T_a) = \SI{2.944}{\volt}$ (Avg Y, V(RC: 1)) e $V_C(T_q) = \SI{2.929}{\volt}$ (Avg Y, V(RC: 1)).}
  \label{fig:sim-ce-ac-sig}
\end{figure}