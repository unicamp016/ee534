\newcommand{\Vp}{V^+}
\newcommand{\Vn}{V^-}
\newcommand{\Vinp}{V_{\mathrm{IN}}^+}
\newcommand{\Vinn}{V_{\mathrm{IN}}^-}
\newcommand{\Vinpmax}{V_{\mathrm{IN}_{\max}}^+}
\newcommand{\Vinnmin}{V_{\mathrm{IN}_{\min}}^-}
\newcommand{\Vinnmax}{V_{\mathrm{IN}_{\max}}^-}
\newcommand{\DVinmax}{\Delta V_{\mathrm{IN}_{\max}}}
\newcommand{\DVinmin}{\Delta V_{\mathrm{IN}_{\min}}}

\section{Projeto}

Nesta seção, vamos projetar um circuito de condicionamento de sinais para
converter a variação de resistência de um termistor considerando um range de
temperatura de ${T_{\min}=\SI{10}{\celsius}}$ a ${T_{\max}=\SI{60}{\celsius}}$
em um sinal de tensão com variação de ${V_{\min} = \SI{0}{\volt}}$ a ${V_{\max}
= \SI{5}{\volt}}$, usando todo range do conversor analógico digital do Arduino
Uno~\cite{arduino}. Para o projeto, vamos usar o amplificador de instrumentação
INA122~\cite{ina122}.

\subsection{Circuito}\label{sec:circuit-design}
A fórmula do ganho do amplificador INA122 é dada pela Equação~\ref{eq:gain},
onde $R_G$ é a resistência aplicada entre os terminais 1 e 8 do amplificador.

\begin{equation}\label{eq:gain}
G = 5 + \frac{\SI{200}{\kilo\ohm}}{R_G}
\end{equation}

A saída do amplificador é dada pela Equação~\ref{eq:output}, onde $\Vinn$ e
$\Vinp$ são as tensões de entrada aplicadas nos terminais 2 e 3 do amplificador,
respectivamente.

\begin{equation}\label{eq:output}
  V_O = G (\Vinp - \Vinn)
\end{equation}

Visto que o amplificador será alimentado com a fonte \SI{5}{\volt}DC do Arduino,
de forma que ${\Vn = \SI{0}{\volt}}$ ${\Vp = \SI{5}{\volt}}$, devemos considerar
no projeto os limites para o sinal de entrada dados pelo datasheet~\cite{ina122}
de $\Vinnmin \ge \Vn - \SI{0,3}{\volt} = \SI{-0,3}{\volt}$ e $\Vinpmax \le \Vp +
\SI{0,3}{\volt} = \SI{5,3}{\volt}$.

Adaptando o circuito sugerido do enunciado, montamos o circuito da
Figura~\ref{fig:circuit}, onde o trimpot foi substituído por uma resistor
convencional.  Para a tensão $V_{CC}$, usamos a tensão de \SI{5}{\volt}DC do
Arduino. Para os capacitores em paralelo, usamos $C_1 = C_2 =
\SI{47}{\micro\farad}$, que ajuda a eliminar o ruído analógico da fonte DC\@.

\begin{figure}
  \centering
  \includegraphics[width=\textwidth]{images/thermistor.pdf}
  \caption{Circuito projetado para o condicionamento do sinal de um termistor
  para o conversor AD do Arduino.}\label{fig:circuit}
\end{figure}

Para começar o projeto, podemos definir um valor para o resistor $R_G$. Seja
$R_G = \SI{200}{\kilo\ohm}$, temos que o ganho do amplificador vale $G = 6$ pela
Equação~\ref{eq:gain}. Para usar todo o range do conversor AD do Arduino,
devemos ter que $V_{O_{\min}} = \SI{0}{\volt}$ e $V_{O_{\max}} = \SI{5}{\volt}$,
de onde temos que $\DVinmin = \frac{V_{O_{\min}}}{G} = \SI{0}{\volt}$ e
$\DVinmax = \frac{V_{O_{\max}}}{G} \approx \SI{833}{\milli\volt}$. O resistor
$R_T$ representa a resistência do termistor, que varia com a temperatura.
Queremos escolher $R_1$ de forma que $\DVinmax \approx \SI{833}{\milli\volt}$.
Considerando que o circuito deve suportar um range de temperatura de
\SI{10}{\celsius} a \SI{60}{\celsius} e usando a relação de resistência por
temperatura dada no enunciado, temos que $R_{T_{\max}} = \SI{4774}{\ohm}$ e
$R_{T_{\min}} = \SI{741}{\ohm}$. Se considerarmos a tensão no terminal 3 do
INA122 fixa, devemos projetar o terminal 2 para variar $\DVinmax$ com a variação
na resistência. Essa relação é dada pela Equação~\ref{eq:r1}, que resolvemos
para encontrar que $R_1 \approx \SI{18,5}{\kilo\ohm}$.

\begin{equation}\label{eq:r1}
  \DVinmax = V_{CC} (\frac{R_{T_{\max}}}{R_{T_{\max}} + R_1} -
  \frac{R_{T_{\min}}}{R_{T_{\min}} + R_1})
\end{equation}

Ou seja, $\Vinnmin =
\SI{5}{\volt}\frac{\SI{741}{\ohm}}{\SI{741}{\ohm} + \SI{18.5}{\kilo\ohm}}
\approx \SI{193}{\milli\volt}$ e $\Vinnmax =
\SI{5}{\volt}\frac{\SI{4774}{\ohm}}{\SI{4774}{\ohm} + \SI{18.5}{\kilo\ohm}}
\approx \SI{1.026}{\volt}$. Se fixarmos, $\Vinp = \Vinnmax$, teremos que para
$T_{\min} = \SI{10}{\celsius}$, $\DVinmin = \Vinp - \Vinnmax = \SI{0}{\volt}$ e
para $T_{\max} = \SI{60}{\celsius}$, $\DVinmax = \Vinp - \Vinnmin =
\SI{833}{\milli\volt}$, como queríamos projetar.

Dessa forma, basta escolher $R_2$ e $R_3$ tal que $\Vinp = \Vinnmax \approx
\SI{1.02}{\volt}$.  Para isso, escolhemos $R_2 = \SI{3,9}{\kilo\ohm}$ e $R_3 =
\SI{1}{\kilo\ohm}$.

\subsection{Cálculo algébrico}

Queremos encontrar a relação entre $V_O$ e a resistência do termistor $R_T$.
Considerando o circuito da Figura~\ref{fig:circuit},
calculamos a expressão algébrica que relaciona $R_T$ e $V_O$ dada pela
Equação~\ref{eq:algebraic-vo}.

\begin{equation}\label{eq:algebraic-vo}
  V_O = V_{CC} (5 + \frac{\SI{200}{\kilo\ohm}}{R_G}) (\frac{R_3}{R_3 + R_2} -
  \frac{R_T}{R_T + R_1}).
\end{equation}

A expressão numérica pode ser derivada a partir do projeto da
Seção~\ref{sec:circuit-design}, de onde temos que

\begin{equation}\label{eq:numeric-vo}
  V_O = \SI{30}{\volt}(\SI{0.204} - \frac{R_T}{R_T + \SI{18.5}{\kilo\ohm}}).
\end{equation}
