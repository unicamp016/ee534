\section{Experimento}\label{sec:experiment}

O circuito da Figura~\ref{fig:circuit} montado é dado pela
Figura~\ref{fig:experiment}, onde um diodo de Zener com $V_z = \SI{5.1}{\volt}$
foi colocado em paralelo com a saída  $V_O$ para limitar a tensão de saída em
$V_z$ e não danificar o conversor DA\@.

\begin{figure}
  \centering
  \includegraphics[width=\textwidth]{images/experiment.jpg}
  \caption{Circuito da Figura~\ref{fig:circuit} montado na protoboard. Na
  figura, a alimentação do circuito foi omitida para facilitar a visualização e
  consiste em \SI{5}{\volt}DC do Arduino aplicado à trilha positiva e o GND à
  trilha negativa.}\label{fig:experiment}
\end{figure}

\subsection{Avaliação qualitativa}

Para avaliar o range de saída do circuito, tomamos duas amostras da tensão $V_O$
usando o oscilloscópio DIY\@ e medimos a resistência $R_T$ do termistor usando
um multímetro. A primeira medida foi tomada com o termistor em temperatura
ambiente, onde obteve-se o resultado da Figura~\ref{fig:scope-room}. A segunda
medida foi feita após pressionar o termistor com o polegar e o indicador para
aquecê-lo, obtendo-se o resultado da Figura~\ref{fig:scope-finger}.

Podemos validar o projeto verificando as tensões $V_O$ projetadas pela
Equação~\ref{eq:numeric-vo} com as tensões medidas na Figura~\ref{fig:scope}.
Para a medida em temperatura ambiente, esperava-se uma tensão de saída $V_O =
\SI{3.233}{\volt}$ e mediu-se que $V_{O_{amb}} = \SI{3.260}{\volt}$, obtendo-se
um erro relativo de ${\frac{V_{O_{amb}} - V_O}{V_O} \cdot \SI{100}{\percent}
\approx \SI{0,8}{\percent}}$.
Também é possível verificar se o ganho do amplificador é próximo do projetado
$G = 6$, calculando-o a partir das tensões $\Vinp$ e $\Vinn$. Temos que ${G_{amb}
= \frac{V_{O_{amb}}}{V_{\mathrm{IN}_{amb}}^+ - V_{\mathrm{IN}_{amb}}^-} \approx
\num{6.07}}$, que depende somente da resistência $R_G$. Esse erro no ganho pode
ser atribuído ao erro no valor nominal da resistência $R_G$ utilizada, bem como
às incertezas das medidas de tensão do oscilloscópio.

Para a medida feita aplicando calor com o dedo, obteve-se que $V_{O_{dedo}} =
\SI{3,905}{\volt}$, enquanto esperava-se que $V_{O} = \SI{3,856}{\volt}$. O erro
relativo maior, valendo cerca de \SI{1,2}{\percent}, pode ser atribuído ao
método experimental, que não garante que a mesma resistência medida para o
termistor com o multímetro estará presente no circuito, visto que o termistor
pode ter aquecido mais após passar mais tempo entre os dedos. O ganho para este
experimento vale $G_{dedo} \approx \num{6.01}$ e é muito mais próximo do
projetado, sinal que a maior fonte de erro vem da incerteza nas medidas feitas
com o oscilloscópio.

Na Figura~\ref{fig:scope}, podemos ver que a tensão de saída $V_O$ apresenta
muito um pouco de ruído, apesar de não ser possível observar o ruído nas tensões
de entrada $\Vinp$ e $\Vinn$. O ruído na entrada não é visível devido à baixa
resolução do osciloscópio DIY, que vale cerca de \SI{4,9}{\milli\volt}. O fato
do ruído tornar-se visível na tensão de sáida apesar do uso de um amplificador
de instrumentação nos diz que o resultado seria muito pior se usássemos um
amplificador operacional, que não tem um CMRR tão alto.

% Talvez dicsutir como chegar na equação da reta de V_O x R_T a partir das duas
% medidas e comparar com a esperada.

\begin{figure}
  \centering
  \begin{subfigure}{0.40\textwidth}
    \includegraphics[width=\textwidth]{images/room-temp-2.png}
    \caption{$R_T = \SI{1970}{\ohm}$}\label{fig:scope-room}
  \end{subfigure}
  \begin{subfigure}{0.40\textwidth}
    \includegraphics[width=\textwidth]{images/finger-temp-3.png}
    \caption{$R_T = \SI{1510}{\ohm}$}\label{fig:scope-finger}
  \end{subfigure} \\ \vspace{0.5em}
    \caption{Medidas feitas com o oscilloscópio para observar o comportamento do
    circuito de condicionamento de sinal da Figura~\ref{fig:experiment}. Nas
    figuras, temos $V_O$ em vermelho, $\Vinp$ em verde e $\Vinn$ em azul. Para
    $R_T = \SI{1970}{\ohm}$~\subref{fig:scope-room}, mediu-se que $V_{O_{amb}} =
    \SI{3.260}{\volt}$, $V_{\mathrm{IN}_{amb}}^+ = \SI{1.011}{\volt}$,
    $V_{\mathrm{IN}_{amb}}^- = \SI{474}{\milli\volt}$. Para $R_T =
    \SI{1510}{\ohm}$~\subref{fig:scope-finger}, mediu-se que $V_{O_{dedo}} =
    \SI{3.905}{\volt}$, $V_{\mathrm{IN}_{dedo}}^+ = \SI{1.016}{\volt}$,
    $V_{\mathrm{IN}_{dedo}}^- = \SI{366}{\milli\volt}$.}
    \label{fig:scope}
\end{figure}

O gráfico da Figura~\ref{fig:temp-over-time} mostra o comportamento da
temperatura no termistor em função do tempo, medido através da tensão na saída
$V_O$ do amplificador de instrumentação. É interessante notar que após aquecer o
termistor com o dedo, a tensão decai exponencialmente para se estabilizar na
temperatura ambiente. Isso é fruto de duas relações exponencias, o aumento
exponencial da resistência $R_T$ em função do decaimento da temperatura $T$ dado
pelo gráfico do enunciado, e o decaimento exponencial da temperatura de um corpo
para se estabilizar com a temperatura do ambiente, prevista pela Lei de
Resfriamento de Newton~\cite{newton}. As oscilações nas medidas de temperatura
no tempo, como a queda brusca na tensão próxima da amostra 460, mostram como o
conversor AD do Arduino está sujeito a grandes incertezas nas medidas. As
oscilações na tensão ao apertar o termistor com o dedo e a demora para
estabilizar também justifica o erro relativo maior em relação à medida feita em
temperatura ambiente, visto que há uma demora para a temperatura se estabilizar
e mesmo depois de um tempo, ela ainda está sujeita a oscilações.

\begin{figure}
  \centering
  \includegraphics[width=\textwidth]{images/temp-over-time.png}
  \caption{Gráfico da tensão de saída $V_O$ ao longo do tempo obtida com o
  conversor AD do Arduino. As primeiras 100 amostras foram tomadas em
  temperatura ambiente. O pico de tensão no gráfico foi devido ao aquecimento do
  termistor ao apertá-lo com o polegar e o dedão. O termistor foi aquecido até
  a temperatura se estabilizar em cerca de \SI{3,9}{\volt} e depois foi solto. O
  restante das amostras indicam o comportamento do termistor para se estabilizar
  na temperatura ambiente.}\label{fig:temp-over-time}
\end{figure}

Por fim, também foram feitos experimentos qualitativos para testar os limites do
projeto. Para resfriar o termistor, aproximamos um cubo de gelo até a tensão de
saída atingir cerca de \SI{1}{\volt}. Para esquentá-lo, aproximamos o ferro de
solda até a tensão de saída saturar em \SI{5}{\volt}.

\subsection{Acendendo um LED}\label{sec:led}

Para acender um LED a partir de determinada temperatura, escolhemos um limiar
que tornasse possível acendê-lo apenas apertando o termistor com os dedos.
Usando as medidas da Figura~\ref{fig:scope}, escolhemos como limiar uma tensão
de aproximadamente \SI{3,5}{\volt}, que é convertida para um valor digital de
${\lfloor \frac{\SI{3,5}{\volt} \cdot 1023}{\SI{5}{\volt}} \rfloor = \num{716}}$
no Arduino.

Colocando o trecho de código da Listagem~\ref{list:led} no firmware do Arduino e
adicionando um LED vermelho em série com \SI{200}{\ohm} na saída do pino 13, foi
possível acender e apagar o LED apertando e soltando o termistor com o dedo,
respectivamente.

\begin{lstlisting}[
  language=C,
  caption=Código em C usado para acender um LED conectado ao o pino digital 13
    do Arduino a partir de determinada temperatura medida através da tensão de
    saída $V_O$ no pino A0.,
  captionpos=b,
  label={list:led}
]
int read_0 = analogRead(0)
if (read_0 >= 716) {
  digitalWrite(13, HIGH);
} else {
  digitalWrite(13, LOW);
}
\end{lstlisting}

\subsection{Esquemático do aquecedor}

O sinal digital do Arduino é uma tensão de \SI{0}{\volt} ou \SI{5}{\volt} que
pode ser acionada pelo firmware do Arduino, como foi feito para acender o LED a
partir de certa temperatura na Seção~\ref{sec:led}. Para acionar um aquecedor de
\SI{100}{\watt}, podemos usar um circuito com relé como chave. O
relé de 5V com 5 pinos da Songle Relay~\cite{relay} suporta até
\SI{10}{\ampere} em uma tensão de \SI{250}{\volt}AC\@ e pode ser acionado com
uma tensão de trigger de \SI{5}{\volt}DC\@.


No circuito da Figura~\ref{fig:heater}, conectamos as duas extremidades da
bobina, responsáveis por acionar a chave do relé, no sinal do pino 13 do Arduino
e o terra. O pino comum do relé foi conectado ao terra e como queremos acionar o
aquecedor apenas quando o sinal do arduino for de \SI{5}{\volt}, conectamos ele
no terminal Normalmente Aberto (NA) do relé. Dessa forma, quando o Arduino
mandar um sinal de \SI{5}{\volt} no pino 13, o relé passará a conduzir corrente
na malha do aquecedor. Dada uma fonte de alimentação da rede elétrica de
\SI{220}{\volt}AC e um aquecedor de \SI{100}{\watt}, podemos aproximar o
aquecedor por uma resistência de
${\frac{\SI{100}{\watt}}{\num{220}^2\si{\volt\squared}} = \SI{484}{\ohm}}$ em
série com a fonte. Assim, a corrente que passa pelo relé é de
aproximadamente \SI{455}{\milli\ampere}, que está dentro do limite de
\SI{10}{\ampere} da peça.

\begin{figure}
  \centering
  \includegraphics[width=0.7\textwidth]{images/heater.pdf}
  \caption{Esquemático do circuito para acionar um aquecedor, representado por
  uma resistência de \SI{484}{\ohm}, usando a saída do pino digital 13 do
  Arduino.}\label{fig:heater}
\end{figure}

% Usar relé como chave e P = V^2 / R para calcular R do aquecedor.
