# Arduino config
BOARD="arduino:avr:uno"
PORT="/dev/ttyACM0"

# Directories
ARDUINO_DIR="scope2"
WEB_APP_DIR="web-app"

# Files
WEB_APP_MAIN="app.py"

# Python full path
PYTHON=$(which python)

.PHONY: build upload connected init-scope

connected:
	@test -c $(PORT) || (echo "Connect arduino to port $(PORT) before proceeding." && exit 1)

build:
	@arduino-cli compile -b $(BOARD) $(ARDUINO_DIR)

upload: connected build
	@arduino-cli upload -b $(BOARD) -p $(PORT) $(ARDUINO_DIR)

init-scope: connected
	sudo $(shell which python) $(WEB_APP_DIR)/$(WEB_APP_MAIN)

